import numpy as np
from intra.signal import IntraSignal
from structures.data import Prediction
"""
	Implementation of HEVC Intra prediction modes

	TODO: pre- and post-filtering process not implemented
	TODO: filtering methods could be private. Only candidate(params) method needs to be exposed as an interface
"""
class DC:
	def __init__(self, sampleFilter=False):
		self.filter = sampleFilter
		self.signal = IntraSignal(0)

	def candidate_filter(self, candidate, ref_above, ref_left, dc):
		"""
			candidate post-filtering process
		"""
		candidate[0][0] = ((ref_left[1] + ref_above[1] + 2 * dc + 2) / 4)
		for l in range(1, candidate): #calc as Vertical mode 
			candidate[0][l] = ((3 * dc + ref_above[l] + 2) / 4)
			candidate[l][0] = ((3 * dc + ref_left[l]  + 2) / 4)

	def candidate(self, region, ref_above, ref_left):
		"""
			Generates the HEVC prediction for Intra DC candidate
		"""
		w, h = region.width, region.height
		total_samples = 2*w*h
		dc = w + np.sum(ref_above) + np.sum(ref_left)
		dc = (dc/total_samples).astype(int)
		candidate = np.full((h, w), dc)

		if self.filter:
			self.candidate_filter(candidate, ref_above, ref_left, dc)

		return Prediction(signal=self.signal, candidate=candidate)

class Planar:
	def __init__(self, sampleFilter=False):
		self.filter = sampleFilter
		self.signal = IntraSignal(1)

	# [TODO] implement filtering process
	# def filter(self, candidate, ref_above, ref_left):
	# 	pass

	def candidate(self, region, ref_above, ref_left):
		"""
			Generates the HEVC prediction for Intra Planar candidate
		"""
		size = region.width
		candidate = np.ndarray(shape=(size, size))
		ph = np.ndarray(shape=(size, size))
		pv = np.ndarray(shape=(size, size))

		for x in range(0, size):
			for y in range(0, size):
				ph[x][y] = (size - 1 - x) * ref_left[y+1] + (x + 1) * ref_above[size+1]
				pv[x][y] = (size - 1 - y) * ref_above[x+1] + (y + 1) * ref_left[size+1]

		for x in range(0, size):
			for y in range(0, size):
				candidate[x][y] = ((ph[x][y] + pv[x][y] + size) / (2 * size)).astype(int)
		
		# if self.filter:
		# 	self.filter(candidate, ref_above, ref_left, dc)

		return Prediction(signal=self.signal, candidate=candidate)

# tables and values from HEVC book
a_values = [32, 26, 21, 17, 13, 9, 5, 2, 0, -2, -5, -9, -13, -17, -21, -26]
# map each mode to its respective angle
a_map = {x:y for x, y in zip([i for i in range(2, 18)], a_values)} #horizontal modes
a_map.update({x:-y for x, y in zip([i for i in range(18, 34)], a_values)}) #vertical modes
a_map.update({34:32}) #last mode (outside horizontal table)
# map each angle A to its inverse B
b_map = {
	-32:-256,
	-26:-315,
	-21:-390,
	-17:-482,
	-13:-630,
	-9:-910,
	-5:-1638,
	-2:-4096
}

class Angular:

	def __init__(self, mode, sampleFilter=False):
		self.filter = sampleFilter
		self.signal = IntraSignal(mode)
		self.a = a_map[mode]
		self.b = b_map[self.a] if self.a<0 else 0
		self.candidate = self.intra_interpolation_horizontal if mode < 18 else self.intra_interpolation_vertical

	def access_elements(self, idx, ref, additional):
		"""
			method to access the reference sample vectors
			if idx<0, it returns the expected sample from the additional vector
		"""
		# idx: index to access ref vector
		# ref: base reference vector
		# additional: complements ref vector when idx<0
		# return: value at position idx, indexing additional when idx<0 according to HM specifications
		if idx>0:
			return ref[idx]
		idx = (idx*self.b + 128)>>8
		return additional[idx]

	def intra_interpolation_vertical(self, region, ref_above, ref_left):
		"""
			Generates the HEVC prediction for Intra Angular Vertical candidate
				w.r.t. horizontal: change reference vectors and candidate indexing generation
		"""
		# size: size of blocks
		# ref: base reference vector
		# additional: complements ref vector when idx<0
		# return: predictor block considering the parameters above described
		size = region.width
		a = self.a
		ref = ref_above
		additional = ref_left

		candidate = np.ndarray(shape=(size, size))
		for y in range(0, size): #calc as Vertical mode 
			for x in range(0, size):
				i = ((y+1)*a)>>5 #integer_displacement
				f = ((y+1)*a)&31 #fractional_displacement
				ref1_idx = x+i+1
				ref2_idx = x+i+2
				ref1 = self.access_elements(ref1_idx, ref, additional)
				ref2 = self.access_elements(ref2_idx, ref, additional) if f!=0 else 0
				candidate[y][x] = ((32-f)*ref1 + f*ref2 + 16) >>5
		
		# if self.filter:
		# 	self.filter(candidate, ref_above, ref_left)

		return Prediction(signal=self.signal, candidate=candidate)

	def intra_interpolation_horizontal(self, region, ref_above, ref_left):
		"""
			Generates the HEVC prediction for Intra Angular Horizontal candidate
				w.r.t. vertical: change reference vectors and candidate indexing generation
		"""
		# size: size of blocks
		# ref: base reference vector
		# additional: complements ref vector when idx<0
		# return: predictor block considering the parameters above described
		size = region.width
		a = self.a

		ref = ref_left
		additional = ref_above

		candidate = np.ndarray(shape=(size, size))
		for y in range(0, size): #calc as Vertical mode 
			for x in range(0, size):
				i = ((y+1)*a)>>5 #integer_displacement
				f = ((y+1)*a)&31 #fractional_displacement
				ref1_idx = x+i+1
				ref2_idx = x+i+2
				ref1 = self.access_elements(ref1_idx, ref, additional)
				ref2 = self.access_elements(ref2_idx, ref, additional) if f!=0 else 0
				candidate[x][y] = ((32-f)*ref1 + f*ref2 + 16) >>5

		# if self.filter:
		# 	self.filter(candidate, ref_above, ref_left)

		return Prediction(signal=self.signal, candidate=candidate)

	# def filter(self, candidate, ref_above, ref_left):
	# 	pass
