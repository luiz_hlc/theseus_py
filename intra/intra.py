import numpy as np
from intra.hevc_modes import DC, Planar, Angular
from structures.data import Prediction


class IntraCore:
	"""
		Performs Intra prediction considering a given evaluation metric
	"""
	def __init__(self, metric):
		self.metric = metric
		self.modes = [DC(), Planar()]#+ [Angular(i) for i in range(2,16)] + [Angular(i) for i in range(16,35)]

	
	def evaluate(self, original_blk, prediction, best):
		"""
			Evaluate a candidate and update the best prediction (if neccessary)
		"""

		#TODO: Change metric signature do (original_blk, prediction)
		#j_cost may consider the signal cost too
		#for now, just consider the distortion value
		distortion = self.metric(original_blk, prediction.candidate)
		prediction.cost = distortion
		best.update_if_better(prediction)
	
	def reference_vectors(self, region, rec_frame):
		"""
			Generate the reference vectors using the reconstructed frame data
				could be a private function
		"""
		#TODO: this functions was implemented assuming fixed region sizes
		w, h = region.width, region.height
		x, y = region.x, region.y
		ref_size = 2*w+1
		nominal_value = 255>>1 #nominal value [TODO: calculate using the bitdepth as rec_frame]
		ref_above = np.full((ref_size), nominal_value)
		ref_left = np.full((ref_size), nominal_value)

		if x==0 and y==0: #top left corner
			return ref_left, ref_above

		if x==0: #first column
			ref_above[1:w+1] = rec_frame[y-1, 0:w] #copy N above samples
			ref_above[w+1:] = ref_above[w] #replicate N samples [TODO: check availability and copy]
			ref_above[0] = ref_above[1] #copy corner sample 
			ref_left = np.full(ref_size, ref_above[0]) #replicate 2N+1 samples from above corner
			return ref_left, ref_above

		if y==0: #first row (similar to x==0 case)
			ref_left[1:w+1] = rec_frame[0:w, x-1]
			ref_left[w+1:] = ref_left[w] 
			ref_left[0] = ref_left[1] 
			ref_above = np.full(ref_size, ref_left[0]) 
			return ref_left, ref_above

		# combination of both cases
		ref_above[:w+1] = rec_frame[y-1, x-1:x+w]
		ref_above[w+1:] = ref_above[w]
		ref_above[0] = rec_frame[y-1][x-1]
		ref_left[:w+1] = rec_frame[y-1:y+w, x-1]
		ref_left[w+1:] = ref_left[w]
		ref_left[0] = rec_frame[y-1][x-1]
		
		return ref_left, ref_above

	def predict(self, region, original_blk, ref_frame): #Forward?
		"""
			Generate the reference vectors using the reconstructed frame data
		"""
		ref_left, ref_above = self.reference_vectors(region, ref_frame)
		best = Prediction()
		
		for mode in self.modes:
			prediction = mode.candidate(region, ref_above, ref_left)
			self.evaluate(original_blk, prediction, best)
		return best.candidate

	def decode(self, signal, region, ref_frame): #Inverse?
		"""
			Reconstruct the reference block using the decoded signals
		"""
		ref_left, ref_above = self.reference_vectors(region, ref_frame)
		mode = signal.mode
		if mode == 0:
			return self.dc_predictor.candidate(region, ref_above, ref_left)
