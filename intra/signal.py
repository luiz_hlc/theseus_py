class IntraSignal:
	"""
		Neccessary sinalization to create the Intra reference block
		Notice that pre- and post- filtering signal are setup for the whole sequence and are not transmitted at block level.
	"""
	def __init__(self, mode):
		self.mode = mode