# def test1():
import numpy as np
import time
from multiprocessing import Pool, RawArray, Process, Pipe

# A global dictionary storing the variables passed from the initializer.
var_dict = {}

def init_worker(X, X_shape):
    # Using a dictionary is not strictly necessary. You can also
    # use global variables.
    var_dict['X'] = X
    var_dict['X_shape'] = X_shape

def worker_func(i):
    # Simply computes the sum of the i-th row of the input matrix X
    X_np = np.frombuffer(var_dict['X']).reshape(var_dict['X_shape'])
    time.sleep(1) # Some heavy computations
    return np.sum(X_np[i,:]).item()

def worker_func_w(data, shape, i):
    # Simply computes the sum of the i-th row of the input matrix X
    # X_np = np.frombuffer(data, dtype='i').reshape(shape)

    # time.sleep(1) # Some heavy computations
    data.data[i][0] = data.data[i][0]*2

def worker_func_test(teste, i):
    idx = teste.idx
    teste.idx = idx+str(i)

    # Simply computes the sum of the i-th row of the input matrix X
    # X_np = np.frombuffer(data, dtype='i').reshape(shape)

    # time.sleep(1) # Some heavy computations
    teste.shr_data.data[i][0] = teste.shr_data.data[i][0]*2
    teste.np_data[i][1] = teste.np_data[i][1]*3

# We need this check for Windows to prevent infinitely spawning new child
# processes.
class SharedContainer: 
    def __init__(self, shape, dtype='i', copy=False, np_data=None):
        self.shape = shape
        self.dtype = dtype 
        self.raw_data = RawArray(dtype, shape[0]*shape[1])
        # Wrap raw_data as an numpy array so we can easily manipulate its data.
        # Copy data to our shared array.
        # return self

    # def fromRaw(self, shape, raw_data):
    #     self.shape = shape
    #     self.raw_data = raw_data
    #     # Wrap raw_data as an numpy array so we can easily manipulate its data.
    #     self.np_data = np.frombuffer(self.raw_data).reshape(shape)
    #     # Copy data to our shared array.
    #     return self

    @property
    def data(self):
        return np.frombuffer(self.raw_data, dtype = self.dtype).reshape(self.shape)

class Teste:
    def __init__(self, idx, shr_data, np_data):
        self.idx = idx
        self.shr_data = shr_data
        self.np_data = np_data


if __name__ == '__main__':
    # X_shape = (16, 1000000)
    # # Randomly generate some data
    # data = np.random.randn(*X_shape)
    # X = RawArray('d', X_shape[0] * X_shape[1])
    # # Wrap X as an numpy array so we can easily manipulates its data.
    # X_np = np.frombuffer(X).reshape(X_shape)
    # # Copy data to our shared array.
    # np.copyto(X_np, data)



    # Start the process pool and do the computation.
    # Here we pass X and X_shape to the initializer of each worker.
    # (Because X_shape is not a shared variable, it will be copied to each
    # child process.)
    # read
    # def test():
    #     with Pool(processes=4, initializer=init_worker, initargs=(X, X_shape)) as pool:
    #         result = pool.map(worker_func, range(X_shape[0]))
    #         print('Results (pool):\n', np.array(result))
    #     # Should print the same results.
    #     print('Results (numpy):\n', np.sum(X_np, 1))
    
    # from multiprocessing import shared_memory
    # # x_data = np.array([[1,2,3,4,5],[6,7,8,9,10], [11,12,13,14,15], [16,17,18,19,20]])
    # x_shape = x_data.shape
    # print(x_shape, x_shape[0] * x_shape[1])
    # # shm = shared_memory.SharedMemory(create=True, size=x_data.nbytes)
    # # new_x_np = np.ndarray(a.shape, dtype=a.dtype, buffer=shm.buf)
    # # x_raw = RawArray('i', x_shape[0] * x_shape[1])
    # # new_x_np = np.frombuffer(x_raw, dtype='i').reshape(x_shape)
    # shr_container = SharedContainer(x_shape, copy=True, np_data=x_data)
    # np.copyto(shr_container.data, x_data)
    # t = Teste("banana", shr_container, x_data)
    # # new_x_np[0][0] = 20

    # # with Pool(processes=4, initializer=init_worker, initargs=(x_raw, x_shape)) as pool:
    # #     pool.map(worker_func_w, range(x_shape[0]))
    # # s0 = Slice(X_np[0,:])
    # # s1 = Slice(X_np[1,:])
    # ps = [Process(target=worker_func_test, args=(t, i)) for i in range(x_shape[0]) ]
    # for p in ps: p.start()
    # for p in ps: p.join() 
    # print(t.idx)
    # print(t.shr_data.data)
    # print(t.np_data)

    class A(Process):
        def __init__(self, tid, conn, threads=None):
            Process.__init__(self)
            self.tid = tid
            self.conn = conn

        def run(self):
            while True:
                frame_id = self.conn.recv()
                print('tid {} received {}'.format(self.tid, frame_id))
                if frame_id == -1:
                    print('tid: {} EXIT'.format(self.tid))
                    exit()
                print('tid: {}, frame:{}'.format(self.tid, frame_id))
                self.conn.send(1)

    class B(Process):
        def __init__(self, tid, conn, threads):
            Process.__init__(self)
            self.n_workers = threads
            self.tid = tid
            self.conn = conn
            self.pipes = [Pipe() for i in range(self.n_workers)]
            self.workers = [A("{}.{}".format(self.tid, i), self.pipes[i][1]) for i in range(self.n_workers)]
            for i in range(self.n_workers): 
                self.workers[i].start()

        def run(self):
            while True:
                frame_id, gopid = self.conn.recv()
                print('tid {} receive {}, {}'.format(self.tid, frame_id, gopid))
                if frame_id == -1:
                    print('tid {} ending childs'.format(self.tid))
                    for i in range(self.n_workers):
                        self.pipes[i][0].send(-1)
                        self.workers[i].join()
                    exit()

                print('tid {} calling childs'.format(self.tid))
                for i in range(self.n_workers):
                    self.pipes[i][0].send(frame_id*self.n_workers + i)

                print('tid {} waiting for childs'.format(self.tid))
                for i in range(self.n_workers):
                    self.pipes[i][0].recv()
                print('tid {} ending'.format(self.tid))
                self.conn.send(1)

workersB = 1
workersA = 2
pipes = [Pipe() for i in range(workersB)]
workers = [B(i, pipes[i][1], workersA) for i in range(workersB)]
for i in range(workersB): 
    workers[i].start()

for i in range(10):
    for j in range(workersB):
        pipes[j][0].send((i*workersB + j, i*j))
    for j in range(workersB):
        pipes[j][0].recv()

for i in range(workersB):
    pipes[i][0].send((-1,-1))
    workers[i].join()
        

