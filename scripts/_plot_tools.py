import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
import matplotlib.ticker as mtick
import matplotlib.patches as mpatches

def autolabel(rects, ax):
    for rect in rects:
        height = rect.get_height()
        ax.text(rect.get_x() + rect.get_width()/2., height,
                '%.2f' % float(height),
                ha='center', va='bottom')

def xdistribution(ngroups, groupsize, width, spacing, offset=0):
    xticks = []
    positions = []
    grouplength = (width + offset) * groupsize
    for n in range(ngroups):
        current = n * (grouplength + spacing) + width/2
        positions += [current + i * (width + offset) for i in range(groupsize)]
        # xticks.append((spacing + grouplength) * n + grouplength / 2 - offset / 2 + width)
        xticks.append(current + (grouplength - offset)/2 -width/2)
    return positions, xticks


def plot_bars(title, labels, x_ticks_labels, y_ticks, data, color, out_path, target_format="png", bar_width=2):
	fig, ax = plt.subplots()
	space_between = 2 # =1: no space
	x_shift = -3

	# n_index = len(data)
	# x_position = np.arange(n_index)
	# x_position = x_position*space_between+x_shift
	x_position, x_ticks = xdistribution(len(x_ticks_labels), 1, bar_width, .75 )

	# # create bars
	bars = ax.bar(x_position, data, bar_width,  ecolor='black', align='edge') #hatch=hatches[i]*4,
	# # ticks
	ax.set_title(title)
	ax.set_xlabel(labels[0])
	ax.set_ylabel(labels[1])
	ax.set_xticklabels(x_ticks_labels )#, rotation=90
	ax.set_yticks(np.arange(y_ticks[0], y_ticks[2], y_ticks[1]))
	plt.tick_params(axis='x', direction='out', top=False)
	plt.tick_params(axis='y', direction='in', right=False)
	ax.grid(color=(.6, .6, .6), linestyle='--', linewidth=.5)
	# ax.set_xticks(x_position)
	ax.set_xticks(x_ticks)

	autolabel(bars, ax)

	print('saving at', out_path)
	plt.show()
	# plt.savefig(out_path+'.'+target_format, format=target_format, bbox_inches='tight', dpi = 500)
	# plt.clf()
	# plt.close('all')

def plot_group_bars(title, labels, x_ticks_labels, group_labels, y_ticks, data, colors, out_path, y_limits='None', target_format="png", bar_width=.75):

	fig, ax = plt.subplots()
	x_position, x_ticks = xdistribution(len(x_ticks_labels), len(group_labels), bar_width, .75 )

	for i, y_data in enumerate(data, 0):
		# print(y_data)
		# print(len(group_labels))
		# print(x_position[i::len(group_labels)])
		bars = ax.bar(x_position[i::len(group_labels)], y_data, bar_width, color=colors[i],  ecolor='black') #hatch=hatches[i]*4,

	ax.tick_params(axis='x', direction='out', top=False)
	ax.tick_params(axis='y', direction='in', right=False)

	ax.set_yticks(np.arange(y_ticks[0], y_ticks[2], y_ticks[1]))
	ax.set_xticks(x_ticks)

	ax.set_xlabel(labels[0])
	ax.set_ylabel(labels[1])
	ax.set_xticklabels(x_ticks_labels)

	if y_limits != 'None':
		ax.set_ylim(y_limits[0], y_limits[1])

	patches = []
	for color, grp_label in zip(colors, group_labels):
		patches.append(mpatches.Patch(fill=True, color=color, label=grp_label))

	ax.legend(handles=patches,bbox_to_anchor=(1.05, 1), loc=2, ncol = 1, borderaxespad=0.)

	print('saving at', out_path)
	plt.savefig(out_path+'.'+target_format, format=target_format, bbox_inches='tight')
	plt.clf()
	plt.close('all')



def add_line(ax, xpos, ypos):
    line = plt.Line2D([xpos, xpos], [ypos + .1, ypos],
                      transform=ax.transAxes, color='black')
    line.set_clip_on(False)
    ax.add_line(line)



# mode_change_art
def plot_stacked_g_bars(title, labels, x_ticks_labels, x_group_labels, stack_labels, y_ticks, data, surfaces, out_path, y_limits='None', target_format="png", bar_width=2):
	# plt.figure()
	fig, ax = plt.subplots(figsize=(5, 3))
	# ax2 = ax.twiny()

	x_position, x_ticks = xdistribution(len(x_group_labels), len(x_ticks_labels), bar_width, .75 )
	x_position_label = [x + bar_width/2 for x in x_position]
	# print("x_position", len(x_position))
	# # create bars
	for i, d in enumerate(data, 0):
		base = [0 for x in range(0, len(x_ticks_labels))]
		for j, line in enumerate(d,0):
			# print(j, line)
			# print(x_position[i*len(x_ticks_labels):(i+1)*len(x_ticks_labels)])
			bars = ax.bar(x_position[i*len(x_ticks_labels):(i+1)*len(x_ticks_labels)], line, bar_width, bottom=base, color=surfaces[j],  ecolor='black', linewidth='.5') #hatch=hatches[i]*4,
			base = [b+y for b,y in zip(base, line)]

	for i in range(1, len(x_group_labels)):
		x_ticks_labels += x_ticks_labels
	# ax.set_ylabel(labels[1])
	ax.set_xticklabels(x_ticks_labels)
	ax.set_yticks(np.arange(y_ticks[0], y_ticks[2], y_ticks[1]))
	plt.tick_params(axis='x', direction='out', top=False)
	plt.tick_params(axis='y', direction='in', right=False)
	ax.set_xticks(x_position_label)

	# remove values in y ticks
	plt.yticks([])

	if y_limits != 'None':
		ax.set_ylim(y_limits[0], y_limits[1])

	ax.set_axisbelow(True)
	for v in np.arange(y_ticks[0], y_ticks[2], y_ticks[1]):
		plt.axhline(y=v, color=(.4,.4,.4), linestyle='--')

	# plt.axes().get_yaxis().set_visible(False)

	# patches = []
	# for s in stack_labels:
	# 	patches.append(mpatches.Patch(fill=True, facecolor =s[1], label=s[0]))
	# ax.legend(handles=patches, loc='upper center', bbox_to_anchor=(.72, 0.9), ncol = 1, borderaxespad=0., fontsize=11)
	# plt.legend(prop={'size': 2})

	# ax.text(-0.1, -0.1, 'level', ha='center', transform=ax.transAxes)
	# ax.text(-0.1, -0.2, 'QP', ha='center', transform=ax.transAxes)
# 
	add_line(ax, 0.04, -.1)
	add_line(ax, 0.04, -.2)

	add_line(ax, 0.46, -.1)
	add_line(ax, 0.46, -.2)
	
	ax.text((.04+.46)/2, -0.2, x_group_labels[0], ha='center', transform=ax.transAxes)

	add_line(ax, 0.54, -.1)
	add_line(ax, 0.54, -.2)
	
	add_line(ax, 0.96, -.1)
	add_line(ax, 0.96, -.2)

	ax.text((0.54+.96)/2, -0.2, x_group_labels[1], ha='center', transform=ax.transAxes)
	# ax2.xaxis.set_ticks_position("bottom")
	# ax2.xaxis.set_label_position("bottom")
	# ax2.spines["bottom"].set_position(("outward", -2))
	# ax2.set_frame_on(True)
	# ax2.patch.set_visible(False)
	# # for sp in ax2.spines.itervalues():
	# #     sp.set_visible(False)
	# # ax2.spines["bottom"].set_visible(True)

	# ax2.set_xticks(x_ticks)
	# ax2.set_xticklabels(x_group_labels)
	# # ax2.set_xlabel(r"Modified x-axis: $1/(1+X)$")

	print('saving at', out_path)
	plt.savefig(out_path+'.'+target_format, format=target_format, bbox_inches='tight')
	# plt.show()
	plt.clf()
	plt.close('all')

# Mode Change Inter
def plot_stacked_bars(title, labels, x_ticks_labels, y_ticks, data, stack_labels, stack_surfaces, out_path, y_limits='None', target_format="png", bar_width=2):
	# plt.figure()
	fig, ax = plt.subplots(figsize=(3.5, 5))

	x_position, x_ticks = xdistribution(len(x_ticks_labels), 1, bar_width, .75 )
	base = [0 for x in x_ticks_labels]
	# create bars
	for i, line in enumerate(data, 0):
		bars = ax.bar(x_position, line, bar_width, bottom=base, color=stack_surfaces[i][0],  edgecolor='black', hatch=stack_surfaces[i][1]) #hatch=hatches[i]*4,
		base = [b+y for b,y in zip(base, line)]

	# ticks
	# ax.set_title(title)
	ax.set_xlabel(labels[0])
	ax.set_ylabel(labels[1])
	ax.set_xticklabels(x_ticks_labels)
	ax.set_yticks(np.arange(y_ticks[0], y_ticks[2], y_ticks[1]))
	# plt.yticks([])
	plt.tick_params(axis='x', direction='out', top=False)
	plt.tick_params(axis='y', direction='in', right=False)
	ax.set_xticks(x_position)

	if y_limits != 'None':
		ax.set_ylim(y_limits[0], y_limits[1])

	ax.set_axisbelow(True)
	for v in np.arange(y_ticks[0], y_ticks[2], y_ticks[1]):
		plt.axhline(y=v, color=(.6,.6,.6), linestyle='--', zorder=-1)

	rstack_labels = stack_labels[::-1]
	rstack_surfaces = stack_surfaces[::-1]
	patches = []
	for lab, surf in zip(rstack_labels, rstack_surfaces):
		patches.append(mpatches.Patch(fill=True, hatch=surf[1], facecolor =surf[0], label=lab))
	ax.legend(handles=patches, loc='upper center', bbox_to_anchor=(1.25, 0.75), ncol = 1, borderaxespad=0.)

	print('saving at', out_path)
	plt.savefig(out_path+'.'+target_format, format=target_format, bbox_inches='tight')
	plt.clf()
	plt.close('all')

#original
# def plot_stacked_bars(title, labels, x_ticks_labels, group_labels, y_ticks, data, surfaces, out_path, y_limits='None', target_format="png", bar_width=2):
# 	# plt.figure()
# 	fig, ax = plt.subplots()

# 	# n_index = len(x_ticks_labels)
# 	# x_position = np.arange(n_index)
# 	# x_position = x_position*space_between+x_shift
# 	x_position, x_ticks = xdistribution(len(x_ticks_labels), 1, bar_width, .75 )
# 	base = [0 for x in x_ticks_labels]
# 	# # create bars
# 	for i, y_data in enumerate(data, 0):
# 		bars = ax.bar(x_position, y_data, bar_width, bottom=base, color=surfaces[i][0],  ecolor='black', hatch=surfaces[i][1]) #hatch=hatches[i]*4,
# 		base = [b+y for b,y in zip(base, y_data)]
# 	# # ticks
# 	ax.set_title(title)
# 	ax.set_xlabel(labels[0])
# 	ax.set_ylabel(labels[1])
# 	ax.set_xticklabels(x_ticks_labels)
# 	ax.set_yticks(np.arange(y_ticks[0], y_ticks[2], y_ticks[1]))
# 	plt.tick_params(axis='x', direction='out', top=False)
# 	plt.tick_params(axis='y', direction='in', right=False)
# 	ax.set_xticks(x_ticks)

# 	if y_limits != 'None':
# 		ax.set_ylim(y_limits[0], y_limits[1])

# 	patches = []
# 	for s, grp_label in zip(surfaces, group_labels):
# 		patches.append(mpatches.Patch(fill=True, hatch=s[1], facecolor =s[0], label=grp_label))
# 	ax.legend(handles=patches, bbox_to_anchor=(1.05, 1), loc=2, ncol = 2, borderaxespad=0.)
	# print('saving at', out_path)
	# plt.savefig(out_path+'.'+target_format, format=target_format, bbox_inches='tight')
	# plt.clf()
	# plt.close('all')


# def plot_stacked_labels(title, labels, x_ticks_labels, group_labels, y_ticks, data, surfaces, out_path, y_limits='None', target_format="png", bar_width=2):
# 	fig = plt.figure(figsize=(.5, .5))
# 	patches = []
# 	for s, grp_label in zip(surfaces, group_labels):
# 		patches.append(mpatches.Patch(fill=True, hatch=s[1], facecolor =s[0], label=grp_label))

# 	fig.legend(patches, group_labels, loc='center', ncol = 6)

# 	print('saving at', out_path)
# 	plt.savefig(out_path+'.'+target_format, format=target_format, bbox_inches='tight')
# 	plt.clf()
# 	plt.close('all')

# # mode_change_art
# def plot_labels(group_labels, surfaces, out_path, target_format="png"):
# 	fig = plt.figure(figsize=(.5, .5))
# 	patches = []
# 	for s, grp_label in zip(surfaces, group_labels):
# 		patches.append(mpatches.Patch(fill=True, facecolor =s, label=grp_label))

# 	fig.legend(patches, group_labels, loc='center', ncol = 3)

# 	print('saving at', out_path)
# 	plt.savefig(out_path+'.'+target_format, format=target_format, bbox_inches='tight')
# 	plt.clf()
# 	plt.close('all')
# mode_change_art

def plot_labels(group_labels, surfaces, out_path, target_format="png"):
	fig = plt.figure(figsize=(.5, .5))
	patches = []
	for s, grp_label in zip(surfaces, group_labels):
		patches.append(mpatches.Patch(fill=True, facecolor =s[0], hatch=s[1], label=grp_label))

	fig.legend(patches, group_labels, loc='center', ncol = 1)

	print('saving at', out_path)
	plt.savefig(out_path+'.'+target_format, format=target_format, bbox_inches='tight')
	plt.clf()
	plt.close('all')























# seems useful

#!/usr/bin/env python

# from matplotlib import pyplot as plt

# def mk_groups(data):
#     try:
#         newdata = data.items()
#     except:
#         return

#     thisgroup = []
#     groups = []
#     for key, value in newdata:
#         newgroups = mk_groups(value)
#         if newgroups is None:
#             thisgroup.append((key, value))
#         else:
#             thisgroup.append((key, len(newgroups[-1])))
#             if groups:
#                 groups = [g + n for n, g in zip(newgroups, groups)]
#             else:
#                 groups = newgroups
#     return [thisgroup] + groups

# def add_line(ax, xpos, ypos):
#     line = plt.Line2D([xpos, xpos], [ypos + .1, ypos],
#                       transform=ax.transAxes, color='black')
#     line.set_clip_on(False)
#     ax.add_line(line)

# def label_group_bar(ax, data):
#     groups = mk_groups(data)
#     xy = groups.pop()
#     x, y = zip(*xy)
#     ly = len(y)
#     xticks = range(1, ly + 1)

#     ax.bar(xticks, y, align='center')
#     ax.set_xticks(xticks)
#     ax.set_xticklabels(x)
#     ax.set_xlim(.5, ly + .5)
#     ax.yaxis.grid(True)

#     scale = 1. / ly
#     for pos in xrange(ly + 1):
#         add_line(ax, pos * scale, -.1)
#     ypos = -.2
#     while groups:
#         group = groups.pop()
#         pos = 0
#         for label, rpos in group:
#             lxpos = (pos + .5 * rpos) * scale
#             ax.text(lxpos, ypos, label, ha='center', transform=ax.transAxes)
#             add_line(ax, pos * scale, ypos)
#             pos += rpos
#         add_line(ax, pos * scale, ypos)
#         ypos -= .1

# if __name__ == '__main__':
#     data = {'Room A':
#                {'Shelf 1':
#                    {'Milk': 10,
#                     'Water': 20},
#                 'Shelf 2':
#                    {'Sugar': 5,
#                     'Honey': 6}
#                },
#             'Room B':
#                {'Shelf 1':
#                    {'Wheat': 4,
#                     'Corn': 7},
#                 'Shelf 2':
#                    {'Chicken': 2,
#                     'Cow': 1}
#                }
#            }
#     fig = plt.figure()
#     ax = fig.add_subplot(1,1,1)
#     label_group_bar(ax, data)
#     fig.subplots_adjust(bottom=0.3)
#     fig.savefig('label_group_bar_example.png')