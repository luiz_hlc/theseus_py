import pandas as pd
import numpy as np


total_exec = 40
video_configs = [
	("BasketballPass_416x240_50", 416, 240)
]

# frames = 100
# raw_path = "/home/sheldon/videos/"
# experiments = [
# 	# ai8: gop_structure = [[0, 7, 3, 1, 2, 4, 5, 6]]
# 	('sequential_ai8', 'theseus_sequential.py', ''),
# 	('fp2_ai8', 'theseus_parallel.py', ' -p 2 -w 1'),
# 	('fp4_ai8', 'theseus_parallel.py', ' -p 4 -w 1'),
# 	('fp6_ai8', 'theseus_parallel.py', ' -p 6 -w 1'),
# 	('fp8_ai8', 'theseus_parallel.py', ' -p 8 -w 1'),
# 	('wpp2_ai8', 'theseus_parallel.py', ' -p 1 -w 2'),
# 	('wpp4_ai8', 'theseus_parallel.py', ' -p 1 -w 4'),
# 	('wpp6_ai8', 'theseus_parallel.py', ' -p 1 -w 6'),
# 	('wpp8_ai8', 'theseus_parallel.py', ' -p 1 -w 8'),
# 	('fp2_wpp2_ai8', 'theseus_parallel.py', ' -p 2 -w 2'),
# 	('fp2_wpp4_ai8', 'theseus_parallel.py', ' -p 2 -w 4'),
# 	('fp2_wpp8_ai8', 'theseus_parallel.py', ' -p 2 -w 8'),
# 	('fp4_wpp2_ai8', 'theseus_parallel.py', ' -p 4 -w 2'),
# 	('fp4_wpp4_ai8', 'theseus_parallel.py', ' -p 4 -w 4'),
# 	('fp4_wpp8_ai8', 'theseus_parallel.py', ' -p 4 -w 8')
# ]


# exec_p_exp = total_exec*len(video_configs)
# total_execs = len(experiments)*exec_p_exp
# total_count = 0
# for exp in experiments:
# 	exp_name = exp[0]
# 	trg_source = exp[1]
# 	flags = exp[2]
# 	log_path = '/home/sheldon/fun/theseus_py/log22/'+exp_name+'/'
# 	if os.path.exists(log_path):
# 		# shutil.rmtree(log_path)
# 		continue
# 	os.mkdir(log_path)

# 	exp_count = 0
# 	for video_cfg in video_configs:
# 		v_name = video_cfg[0]
# 		v_w = video_cfg[1]
# 		v_h = video_cfg[2]
def report_averages(path, video, experiment):
	log_path = path+experiment+'/'+video
	total_time = []
	load_time = []
	frame_time = []
	pred_time = []
	for i in range(total_exec):
		log_file = log_path+'_it{}.txt'.format(i)
		x = pd.read_csv(log_file, sep='\t', header=None, error_bad_lines=False)
		# loadTime_log = x[ x[0].str.contains('load_*') ]
		# load_time.append(loadTime_log[1].sum())
		pred_time = pred_time + x[x[0].str.contains('pred') ][1].tolist()
		frame_time = frame_time + x[x[0].str.contains('f') ][1].tolist()
		total_time.append(x[x[0].str.contains('TotalTime')].iloc[0][1])

	# load_time = pd.Series(load_time)
	total_time = pd.Series(total_time)
	print('#### total_time')
	print(total_time.describe())
	
	frame_time = pd.Series(frame_time)
	print('#### frame_time')
	print(frame_time.describe())

	pred_time = pd.Series(pred_time)
	print('#### pred_time')
	print(pred_time.describe())
	# print('total_tile:{:.2f}\nframe_time:{:.4f}'.format(total_time_avg, pred_avg))

def parse_times(path, experiment, video):
	log_path = path+experiment+'/'+video
	# log_path = '/home/sheldon/fun/theseus_py/log/'+experiment+'.txt'
	total_time = []
	load_time = []
	processing_time = []
	for i in range(total_exec):
		log_file = log_path+'_it{}.txt'.format(i)
		x = pd.read_csv(log_file, sep='\t', header=None, error_bad_lines=False)
		# loadTime_log = x[ x[0].str.contains('load_*') ]
		framesTime_log = x[ x[0].str.contains('f') ]

		# load_time.append(loadTime_log[1].sum())
		processing_time.append(framesTime_log.iloc[0][1])
		total_time.append(x[x[0].str.contains('TotalTime')].iloc[0][1])

	total_time = pd.Series(total_time)
	# load_time = pd.Series(load_time)
	# processing_time = pd.Series(processing_time)
	return total_time, None

def calc_speedup(path, video, baseline, target):
	
	total_a, load_a = parse_times(path, baseline, video)
	total_a_avg = total_a.describe()[1]

	total_b, load_b = parse_times(path, target, video)
	total_b_avg = total_b.describe()[1]

	speed_up = total_a_avg/total_b_avg
	print('speedup: {:.2f}'.format(speed_up))

from optparse import OptionParser
parser = OptionParser()
parser.add_option("-b", "--baseline", dest="baseline", type= "string", metavar="MODE")
parser.add_option("-t", "--target", dest="target", type= "string", metavar="MODE")
parser.add_option("-m", "--mode", dest="mode", type= "string", metavar="MODE")

root_path = '/home/sheldon/fun/paralela/'
log_path = root_path+'logF_240p_ai35c8_4core/'
video = 'BasketballPass_416x240_50'
video = 'BasketballPass'

(options, args) = parser.parse_args()
if options.mode == 'compare':
	calc_speedup(log_path, video, options.baseline, options.target)
elif options.mode == 'report':
	report_averages(log_path, video, options.target)
