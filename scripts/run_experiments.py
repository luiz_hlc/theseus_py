import subprocess
import os
import shutil

total_exec = 1
frames = 8
raw_path = "/home/sheldon/videos/"
experiments = [
	# ai8: gop_structure = [[0, 7, 3, 1, 2, 4, 5, 6]]
	 ('c8p1w1_ai8', 'theseus_sequential.py', ' -c 8'),
	('c8p2w1_ai8', 'theseus_parallel.py', ' -c 8 -p 2 -w 1'),
	('c8p4w1_ai8', 'theseus_parallel.py', ' -c 8 -p 4 -w 1'),
	('c8p8w1_ai8', 'theseus_parallel.py', ' -c 8 -p 8 -w 1')
	 # ('c8p1w2_ai8', 'theseus_parallel.py', ' -c 8 -p 1 -w 2'),
	 # ('c8p1w4_ai8', 'theseus_parallel.py', ' -c 8 -p 1 -w 4'),
	 # ('c8p1w8_ai8', 'theseus_parallel.py', ' -c 8 -p 1 -w 8'),
 	# ('c64p1w1_ai8', 'theseus_sequential.py', ' -c 64'),
	 # ('c64p2w1_ai8', 'theseus_parallel.py', ' -c 64 -p 2 -w 1'),
	 # ('c64p4w1_ai8', 'theseus_parallel.py', ' -c 64 -p 4 -w 1'),
	 # ('c64p1w2_ai8', 'theseus_parallel.py', ' -c 64 -p 1 -w 2'),
	 # ('c64p1w4_ai8', 'theseus_parallel.py', ' -c 64 -p 1 -w 4'),
	# ('c8p8w1_ai8', 'theseus_parallel.py', ' -c 8 -p 8 -w 1'),
	# ('c8p1w8_ai8', 'theseus_parallel.py', ' -c 8 -p 1 -w 8'),
	# ('c8p1w16_ai8', 'theseus_parallel.py', ' -c 8 -p 1 -w 16'),
	# ('c64p8w1_ai8', 'theseus_parallel.py', ' -c 64 -p 8 -w 1'),
	# ('c64p1w8_ai8', 'theseus_parallel.py', ' -c 64 -p 1 -w 8'),
	# ('c64p1w16_ai8', 'theseus_parallel.py', ' -c 64 -p 1 -w 16')
	# ('fp2_wpp2_ai8', 'theseus_parallel.py', ' -p 2 -w 2'),
	# ('fp2_wpp4_ai8', 'theseus_parallel.py', ' -p 2 -w 4'),
	# ('fp2_wpp8_ai8', 'theseus_parallel.py', ' -p 2 -w 8'),
	# ('fp4_wpp2_ai8', 'theseus_parallel.py', ' -p 4 -w 2'),
	# ('fp4_wpp4_ai8', 'theseus_parallel.py', ' -p 4 -w 4'),
	# ('fp4_wpp8_ai8', 'theseus_parallel.py', ' -p 4 -w 8')
]

video_configs = [
	("BasketballPass_416x240_50", 416, 240)
	# ("BasketballPass_416x240_50", 1280, 720)
	# ("BasketballPass", 1920, 1080)
]

exec_p_exp = total_exec*len(video_configs)
total_execs = len(experiments)*exec_p_exp
total_count = 0
for exp in experiments:
	exp_name = exp[0]
	trg_source = exp[1]
	flags = exp[2]
	log_path = '/home/sheldon/fun/theseus_py/log_240p_ai35_frame_rawArray/'+exp_name+'/'
	if os.path.exists(log_path):
		shutil.rmtree(log_path)
		continue
	os.mkdir(log_path)

	exp_count = 0
	for video_cfg in video_configs:
		v_name = video_cfg[0]
		v_w = video_cfg[1]
		v_h = video_cfg[2]

		for i in range(total_exec):
			# i=0
			per_total = 100*(total_count/total_execs)
			per_exp = 100*(exp_count/exec_p_exp)
			print("[{:.2f}%,{:.2f}%]\t{}: {}_it{}".format(per_total, per_exp, exp_name, v_name, i))

			v_file = raw_path+v_name+'.yuv'
			log_file = '{}_it{}.txt'.format(v_name, i)
			command = 'python {} -m e -i {} -W {} -H {} -f {} -b 8 -s 420'.format(trg_source, v_file, v_w, v_h, frames)
			command = command + flags
			command =  command+' >> '+log_path+log_file
			# print(command)
			subprocess.call(command, shell=True)
			
			total_count = total_count+1
			exp_count = exp_count+1
