speedup = {
	'c8p2w1_ai8'	: 1.58 ,
	'c8p4w1_ai8'	: 2.16 ,
	'c8p8w1_ai8'	: 2.23 ,
}

time = {
	'c8p1w1_ai8'	: (92.66, 11.5567),
	'c8p2w1_ai8'	: (58.4841, 14.5646),
	'c8p4w1_ai8'	: (42.9587, 21.3792),
	'c8p8w1_ai8'	: (41.6322, 41.4922)
}



# frame_time = {
# 	'c8p1w1_ai8'	: ,
# 	'c8p2w1_ai8'	: ,
# 	'c8p4w1_ai8'	: ,
# 	'c8p8w1_ai8'	: ,
# }

# block_time = {
# 	'c8p1w1_ai8'	: ,
# 	'c8p2w1_ai8'	: ,
# 	'c8p4w1_ai8'	: ,
# 	'c8p8w1_ai8'	: ,
# }

from _plot_tools import *

def select(dictionary, key_filters, remove=False, exclusive=True):
	key_list = dictionary.keys()
	if exclusive:
		for f in key_filters: key_list = list(filter(lambda k: f in k, key_list))
	else:
		new_list = []
		for f in key_filters: new_list = new_list + list(filter(lambda k: f in k, key_list))
		key_list = new_list

	if remove:
		new_dict = dictionary
		for k in key_list: new_dict.pop(k) 
		return new_dict
	else:
		return dict((k, dictionary[k]) for k in key_list)


import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
import matplotlib.ticker as mtick
import matplotlib.patches as mpatches

def plot_group_bars2(title, labels, bar_labels, group_labels, y_ticks, data, colors, out_path, y_limits='None', target_format="png", bar_width=.75):

	fig, ax = plt.subplots()
	# ax.set_title(title)
	
	x_position, x_ticks = xdistribution(len(group_labels), len(bar_labels), bar_width, .75 )
	for i in range(len(group_labels)):
		begin = i*len(bar_labels)
		end = begin + len(bar_labels)
		bars = ax.bar(x_position[begin:end], data[begin: end], bar_width, color=colors, ecolor='black') #hatch=hatches[i]*4,
		autolabel(bars, ax)

	ax.tick_params(axis='x', direction='out', top=False)
	ax.tick_params(axis='y', direction='in', right=False)
	
	ax.set_axisbelow(True)

	ax.set_yticks(np.arange(y_ticks[0], y_ticks[2], y_ticks[1]))
	ax.set_ylabel(labels[1])
	ax.yaxis.grid(color=(.6, .6, .6), linestyle='--', linewidth=.5)

	ax.set_xlabel(labels[0])
	ax.set_xticks(x_ticks)
	ax.set_xticklabels(group_labels)

	if y_limits != 'None':
		ax.set_ylim(y_limits[0], y_limits[1])

	patches = []
	for color, grp_label in zip(colors, bar_labels):
		patches.append(mpatches.Patch(fill=True, color=color, label=grp_label))

	ax.legend(handles=patches,bbox_to_anchor=(.7, .95), loc=2, ncol = 1, borderaxespad=0.)
	ax.set_ylim([y_ticks[0],y_ticks[-1]]) #[EDIT] see only the region of interest

	# print('saving at', out_path)
	plt.savefig(out_path+'.'+target_format, format=target_format, bbox_inches='tight')
	plt.clf()
	plt.close('all')
	# plt.show()

def plot_bars(title, labels, x_ticks_labels, y_ticks, data, color, out_path, target_format="png", bar_width=2):
	fig, ax = plt.subplots()
	space_between = 2 # =1: no space
	x_shift = -3

	# n_index = len(data)
	# x_position = np.arange(n_index)
	# x_position = x_position*space_between+x_shift
	x_position, x_ticks = xdistribution(len(x_ticks_labels), 1, bar_width, .25)

	# # create bars
	bars = ax.bar(x_position, data, bar_width,  ecolor='black', align='center') #hatch=hatches[i]*4,
	# # ticks
	# ax.set_title(title)
	ax.set_xlabel(labels[0])
	ax.set_ylabel(labels[1])
	ax.set_xticklabels(x_ticks_labels )#, rotation=90
	ax.set_yticks(np.arange(y_ticks[0], y_ticks[2], y_ticks[1]))
	plt.tick_params(axis='x', direction='out', top=False)
	plt.tick_params(axis='y', direction='in', right=False)
	# ax.grid(color=(.6, .6, .6), linestyle='--', linewidth=.5)
	ax.yaxis.grid(color=(.6, .6, .6), linestyle='--', linewidth=.5)
	# ax.set_xticks(x_position)
	ax.set_xticks(x_ticks)
	plt.axhline(y=1, color=(.8,0,0), linewidth=2)

	autolabel(bars, ax)

	print('saving at', out_path)
	# plt.show()
	plt.savefig(out_path+'.'+target_format, format=target_format, bbox_inches='tight', dpi = 500)
	plt.clf()
	plt.close('all')

# filter_list = ['c8', 't41']
# group1 = select(frame_time, filter_list)
# group1.pop('c8p1w1t41')
# group2 = select(frame_time, ['c64', 't41'])
# group2.pop('c64p1w1t41')

title = '4 cores'
file_name = 'plotf_tempos' 
bar_labels = ['Total','Por Quadro']
labels = ['#Quadros Paralelos', 'Tempo (s)']

out_path = '/home/sheldon/fun/paralela/plots/'+file_name
group_labels = ['1', '2', '4', '8']
y_ticks = [0,10,100.1] #frame 
# data = speedup.values()
data = [] 
for t in time.values():
	data = data + [t[0], t[1]]
# print(data)
# data = 
colors = ['#0000a0','#a0a0ff']#,'#ff00ff']
plot_group_bars2(title, labels, bar_labels, group_labels, y_ticks, data, colors, out_path)
# plot_bars(title, labels, bar_labels, y_ticks, data, colors, out_path, bar_width=.5)


