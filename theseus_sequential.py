# command (ctrl+c, ctrl+v)
# python theseus.py -m e -i "/home/sheldon/videos/BasketballPass_416x240_50.yuv" -W 416 -H 240 -f 1 -b 8 -s 420 -c 64 -p 1 -w 1
# python theseus_parallel.py -m e -i "/home/eclvc/RAW/BasketballPass.yuv" -W 416 -H 240 -f 8 -b 8 -s 420 -c 8 -p 1 -w 1 
# python theseus.py -m d -i "encoded.bin"

# numpy use a lot of parallelism in its function
# This disables numpy multithreading
import os
os.environ['MKL_NUM_THREADS'] = '1'

import structures.types as types
import libio.OptionsParser as optParser
import structures.data as data

# [TODO]
# from standard.parallel_model import StandardController as Encoder
from standard.model import StandardController as Encoder



import time
start = time.time()

options = optParser.parse_arguments()
in_filename = options.in_file
out_filename = options.out_file

if options.mode == 'e': #encode
	video_cfg = types.VideoCfg(options.frame_width, options.frame_height, options.frames, options.bit_depth, options.sub_sampling)
	encoder = Encoder(video_cfg, in_filename, out_filename, options.ctuSize, total_f=options.frames,frame_threads=options.frameThreads, wpp_threads=options.wppThreads)
	encoder.run()

elif options.mode == 'd': #decode
	pass
	# test = std.ExampleStandard(out_filename)
	# try:
	# 	while True:
	# 		test.read_yData()
	# except ValueError:
	# 	print("End of File")
end = time.time()
print('TotalTime\t{}'.format(end - start))
