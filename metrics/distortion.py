import numpy as np

def sad(block_a, block_b):
	diff = block_a - block_b
	absolute = np.abs(diff)
	result = np.sum(absolute)
	return result