import numpy as np
from inter.ime import IME
from structures.data import Prediction


class InterCore:
	def __init__(self, metric):
		self.metric = metric
		self.modes = [IME(metric)] #+ [Angular(i) for i in range(2,35)]

	
	def evaluate(self, original_blk, prediction, best):
		#TODO: Change metric signature do (original_blk, prediction)
		#j_cost may consider the signal cost too
		#for now, just consider the distortion value
		distortion = self.metric(original_blk, prediction.candidate)
		prediction.cost = distortion
		best.update_if_better(prediction)

	def predict(self, region, original_blk, ref_frame): #Forward?
		ref_left, ref_above = self.reference_vectors(region, ref_frame)
		best = Prediction()
		
		for mode in self.modes:
			prediction = mode.candidate(region, ref_above, ref_left)
			self.evaluate(original_blk, prediction, best)
		return best.candidate

	def decode(self, signal, region, ref_frame): #Inverse?
		ref_left, ref_above = self.reference_vectors(region, ref_frame)
		mode = signal.mode
		if mode == 0:
			return self.dc_predictor.candidate(region, ref_above, ref_left)



# 	def predict(self, region):
# 		hor_ref, vert_ref = self.generate_references(region)

# 		return signal, cost, samples 





