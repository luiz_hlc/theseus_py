import numpy as np
import structures.data as data

def hadamardMatrix(size):
		if size == 2:
			return np.array([[1,1], [1,-1]])
		half_size = size>>1
		had = np.ones(shape=(size,size))
		half_had = hadamardMatrix(half_size)
		had[0:half_size, 0:half_size] = half_had
		had[0:half_size, half_size:size] = half_had
		had[half_size:size, 0:half_size] = half_had
		had[half_size:size, half_size:size] = -1*half_had
		return had

class Transform_Matrix:
	forward = {4: hadamardMatrix(4), 8: hadamardMatrix(8), 16: hadamardMatrix(16), 32: hadamardMatrix(32), 64: hadamardMatrix(64)}
	inverse = forward

class HadamardTransform:
	class Forward:
		def __init__(self):
			self.matrix = Transform_Matrix.forward

		def function(self, np_array):
			width, height = np_array.shape
			if width != height or height%2 != 0:
				return np_array
			matrix = self.matrix[width]
			return (np.dot(np.dot(matrix, np_array), matrix)/width).astype(dtype=int)

	class Inverse:
		def __init__(self):
			self.matrix = Transform_Matrix.inverse

		def function(self, np_array): 
			width, height = np_array.shape
			if width != height or height%2 != 0:
				return np_array
			matrix = self.matrix[width]
			return (np.dot(np.dot(matrix, np_array), matrix)/width).astype(dtype=int)
