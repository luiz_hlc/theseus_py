import math
from multiprocessing import Process, Semaphore, Pipe
import time
import queue


from standard.frame import stdFrame as Frame
from standard.parser import stdParser as Parser
from standard.partitioner import stdPartitioner as Partitioner
from standard.transform import stdTransform as Transform
from standard.quantization import stdQuantizer as Quantizer
from standard.residue import stdResidualUnity as ResidueUnity
from standard.prediction import stdPredictionUnity as PredictionUnity
from standard.frame_hierarchy import Hierarchy_Controller as Frame_Hierarchy
from standard.parallel import FramePool_controller as WorkerPool


# import cProfile
# def do_cprofile(func):
#     def profiled_func(*args, **kwargs):
#         profile = cProfile.Profile()
#         try:
#             profile.enable()
#             result = func(*args, **kwargs)
#             profile.disable()
#             return result
#         finally:
#             profile.print_stats()
#     return profiled_func

class StandardEncoder:
	def __init__(self, video_cfg, in_filename, out_filename, ctu_size):
		self.v_cfg = video_cfg

		self.parser = Parser(video_cfg, in_filename)
		self.partitioner = Partitioner(video_cfg, ctu_size)

		self.qp_control = Quantizer.Control()

		self.prediction = PredictionUnity.Forward()
		self.resUnity = ResidueUnity.Forward()
		self.transf = Transform.Forward()
		self.quant = Quantizer.Forward(self.qp_control)
		self.inv_quant = Quantizer.Inverse(self.qp_control)
		self.inv_transf = Transform.Inverse()
		self.inv_resUnity = ResidueUnity.Inverse()

	def process_CTU(self, ctu):
		ctu.predict(self.prediction.function)
		ctu.residue(self.resUnity.function)
		ctu.transform(self.transf.function)
		ctu.quantization(self.quant.function)
		ctu.inv_quantization(self.inv_quant.function)
		ctu.inv_transform(self.inv_transf.function)
		ctu.reconstruct(self.inv_resUnity.function)
		
	def process(self,frame):
		start = time.time()
		
		y_ctu_matrix = frame.ctu_matrix[0]
		for line in y_ctu_matrix:
			for yCTU in line:
				rCTU = self.partitioner.ctu_from_region(frame, 0, yCTU)
				self.process_CTU(rCTU)

		end = time.time()
		print('f{}\t{}\t{}\t{}'.format(frame.signals.idx, start, end , end - start))


class StandardController:
	def __init__(self, video_cfg, in_filename, out_filename, ctu_size, total_f=60, frame_threads=1, wpp_threads=1):
		self.total_f = total_f
		self.model = StandardEncoder(video_cfg, in_filename, out_filename, ctu_size)
		self.wpp_threads = wpp_threads

		self.parser = Parser(video_cfg, in_filename)
		self.partitioner = Partitioner(video_cfg, ctu_size)
		self.hierarchy = Frame_Hierarchy(video_cfg, self.partitioner.ctu_soft_split, self.parser.parse)
		self.workerPool = WorkerPool(frame_threads, self.hierarchy.frame_buffer, self.model, wpp_threads)

	def run(self):
		gop_size = self.hierarchy.gop_size
		total_gops = int(math.floor(self.total_f/gop_size))
		gop_structure = self.hierarchy.gop_structure
		
		
		for i in range(total_gops):
			# Parse
			fId_list = self.hierarchy.parse_gop()

			# Process level by level
			gop_count = 0
			for level in gop_structure:
				#start
				level_count = len(level)
				frames2process = fId_list[gop_count : gop_count+level_count]
				self.workerPool.process(frames2process)
				gop_count = gop_count + level_count

		remaining_frames = self.total_f%gop_size
		if remaining_frames != 0:
			# Parse
			fId_list = self.hierarchy.parse_gop(remaining_frames)

			# Process level by level
			gop_count = 0
			for level in gop_structure:
				level_count = len(level)
				if gop_count+level_count >= remaining_frames:
					frames2process = fId_list[gop_count:]
					self.workerPool.process(frames2process)
					break
				else:
					frames2process = fId_list[gop_count : gop_count+level_count]
					self.workerPool.process(frames2process)
					gop_count = gop_count + level_count

		# dbg prediction data
		buff = self.hierarchy.frame_buffer
		for i in range(8):
			buff[i].dbg_pred_channels()

		# finish processes
		self.workerPool.finish()