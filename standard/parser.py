from libio.YuvParser import YuvParser as Parser

class stdParser(Parser): #Source
	def __init__(self, video_cfg, filename):
		super().__init__(video_cfg, filename)

	def parse(self, f_id, buff_channels):
		np_buffers = [b.data for b in buff_channels]
		self.read(f_id, np_buffers)

