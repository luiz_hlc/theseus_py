import time
import math

from standard.frame import stdFrame as Frame
from standard.parser import stdParser as Parser
from standard.partitioner import stdPartitioner as Partitioner
from standard.transform import stdTransform as Transform
from standard.quantization import stdQuantizer as Quantizer
from standard.residue import stdResidualUnity as ResidueUnity
from standard.prediction import stdPredictionUnity as PredictionUnity

from standard.frame_hierarchy import Hierarchy_Controller as Frame_Hierarchy


# import cProfile
# def do_cprofile(func):
#     def profiled_func(*args, **kwargs):
#         profile = cProfile.Profile()
#         try:
#             profile.enable()
#             result = func(*args, **kwargs)
#             profile.disable()
#             return result
#         finally:
#             profile.print_stats()
#     return profiled_func

class StandardEncoder:
	def __init__(self, video_cfg, in_filename, out_filename):
		self.v_cfg = video_cfg
		self.qp_control = Quantizer.Control()

		self.prediction = PredictionUnity.Forward()
		self.resUnity = ResidueUnity.Forward()
		self.transf = Transform.Forward()
		self.quant = Quantizer.Forward(self.qp_control)
		self.inv_quant = Quantizer.Inverse(self.qp_control)
		self.inv_transf = Transform.Inverse()
		self.inv_resUnity = ResidueUnity.Inverse()

	def process_CTU(self, ctu):
		ctu.predict(self.prediction.function)
		ctu.residue(self.resUnity.function)
		ctu.transform(self.transf.function)
		ctu.quantization(self.quant.function)
		ctu.inv_quantization(self.inv_quant.function)
		ctu.inv_transform(self.inv_transf.function)
		ctu.reconstruct(self.inv_resUnity.function)

	def process(self, frame):
		start = time.time()

		y_ctu_matrix = frame.ctu_matrix[0]
		for line in y_ctu_matrix:
			for yCTU in line:
				self.process_CTU(yCTU)

		end = time.time()
		f_id = frame.signals.idx
		print('f{}\t{}\t{}\t{}'.format(f_id,start, end, end - start))


class StandardController:
	def __init__(self, video_cfg, in_filename, out_filename, ctu_size, total_f=60,**kwargs):
		self.total_f = total_f
		self.model = StandardEncoder(video_cfg, in_filename, out_filename)

		self.parser = Parser(video_cfg, in_filename)
		self.partitioner = Partitioner(video_cfg, ctu_size)
		self.hierarchy = Frame_Hierarchy(video_cfg, self.partitioner.ctu_hard_split, self.parser.parse)

	# @do_cprofile
	def run(self):
		gop_size = self.hierarchy.gop_size
		total_gops = int(math.floor(self.total_f/gop_size))
		gop_structure = self.hierarchy.gop_structure
		frame_buffer = self.hierarchy.frame_buffer
		

		for i in range(total_gops):
			# Parse
			fId_list = self.hierarchy.parse_gop()

			# process
			for fIds in fId_list:
				f_id, gop_id = fIds  
				frame = frame_buffer[gop_id]
				self.model.process(frame)
								
		remaining_frames = self.total_f%gop_size
		if remaining_frames != 0:
			# Parse
			fId_list = self.hierarchy.parse_gop(remaining_frames)

			# process
			for fIds in fId_list:
				f_id, gop_id = fIds  
				frame = frame_buffer[gop_id]
				self.model.process(frame)

		# dbg prediction data
		buff = self.hierarchy.frame_buffer
		for i in range(8):
			buff[i].dbg_pred_channels()
