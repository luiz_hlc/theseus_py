from transform.transform import HadamardTransform as Transform
import numpy as np

class stdTransform:
	class Monitor:
		def __init__(self):
			self.values_map = {}
			self.count = {}

		def register(self, np_array):
			width, height = np_array.shape
			if width != height or height%2 != 0:
				return

			if not width in self.values_map:
				self.values_map[width] = np.ndarray((width, width), int)

				self.values_map[width][:][:] = np.absolute(np_array)
				self.count[width] = 0
				return

			self.values_map[width] = self.values_map[width] + np.absolute(np_array)
			self.count[width] = self.count[width] + 1

		def report(self):
			for key in self.values_map:
				print("key: {}".format(key))
				print(self.values_map[key]/self.count[key])

	class Forward:
		def __init__(self):
			self.f = Transform.Forward().function

			self.monitor = stdTransform.Monitor()

		def function(self, in_blk, out_blk):
			out_blk[:][:] = self.f(in_blk)
			# self.monitor.register(out_blk)

	class Inverse:
		def __init__(self):
			self.f = Transform.Inverse().function

		def function(self, in_blk, out_blk):
			out_blk[:][:] = self.f(in_blk)
			# self.monitor.register(out_blk)