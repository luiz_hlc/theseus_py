from intra.intra import IntraCore
from metrics.distortion import sad as eval_metric
import time

class stdPredictionUnity:
	class Forward:
		def __init__(self):
			self.intraPrediction = IntraCore(eval_metric).predict

		def predict(self, original_blk, input_ref, region, output_blk):
			output_blk[:][:] = self.intraPrediction(region, original_blk, input_ref)

		def function(self, original_blk, input_ref, region, output_blk):
			w, h = region.width, region.height
			if w != h:
				# skip
				output_blk[:][:] = original_blk
			else:
				# start = time.time()
				self.predict(original_blk, input_ref, region, output_blk)
				# end = time.time()
				# print('pred\t{}'.format( end - start))
				
	# class Inverse:
	# 	def __init__(self):
	# 		self.f = Residue.Inverse().function

	# 	#TODO: Please change this
	# 	def function(self, input_ori, input_ref, region, output_buffer):
	# 		x_begin = region.x
	# 		x_end = region.x + region.width
	# 		y_begin = region.y
	# 		y_end = region.y + region.height
	# 		output_buffer.data[y_begin:y_end, x_begin:x_end] = self.f(input_ori.data[y_begin:y_end, x_begin:x_end], input_ref.data[y_begin:y_end, x_begin:x_end])
