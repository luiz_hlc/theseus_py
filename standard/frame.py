import numpy as np
from structures.types import Channel

class FrameSignals:
		def __init__(self, idx, yShape, chShape, qp, ref_list):
			self.idx = idx
			self.yShape = yShape
			self.chShape= chShape
			self.qp = qp
			self.ref_list = ref_list

class stdFrame:
	"""stdFrame initializes all necessary buffers
	All data used on encoder are references to attributes from this class
	"""

	def __init__(self, signals, Container_t):
		self.signals = signals

		self.buff_ori_channels = [
			Container_t(signals.yShape),
			Container_t(signals.chShape),
			Container_t(signals.chShape)
		]

		self.ctu_matrix = [[],[],[]]

		self.buff_predicted_channels = [
			Container_t(signals.yShape),
			Container_t(signals.chShape),
			Container_t(signals.chShape)
		]

		self.buff_temp_channels = [
			Container_t(signals.yShape),
			Container_t(signals.chShape),
			Container_t(signals.chShape)
		]

		self.buff_reconstructed_channels = [
			Container_t(signals.yShape),
			Container_t(signals.chShape),
			Container_t(signals.chShape)
		]

	def parse_raw(self, function):
		"""Parses the raw frame data to buff_ori_channels
		Parameters
		----------
		function(frame_idx, buffer)
			Parser function
		"""
		trg = self.buff_ori_channels
		function(self.signals.idx, trg)

	def split_CTUs(self, channel, function):
		""" Creates CTU matrix for channel
		Parameters
		----------
		function(Channel):
			partitioning function
		"""

		self.ctu_matrix[channel] = function(self, channel)





	def dbg_ori_channels(self):
		print('[dbg] ori_channels')
		print('\tframe id: {}'.format(self.signals.idx))
		import matplotlib.pyplot as plt
		color_map = plt.cm.get_cmap('Greys')
		plt.imshow(self.buff_ori_channels[0].data, cmap=color_map.reversed(), interpolation='nearest')
		plt.show()

	def dbg_pred_channels(self, line_idx=0):
		print('[dbg] rec_channels')
		print('\tframe id: {}'.format(self.signals.idx))
		import matplotlib.pyplot as plt
		color_map = plt.cm.get_cmap('Greys')
		plt.imshow(self.buff_predicted_channels[0].data, cmap=color_map.reversed(), interpolation='nearest')
		plt.title(f'Line: {line_idx}')
		plt.show()

	def dbg_temp_channels(self):
		print('[dbg] temp_channels')
		print('\tframe id: {}'.format(self.signals.idx))
		import matplotlib.pyplot as plt
		color_map = plt.cm.get_cmap('Greys')
		plt.imshow(self.buff_temp_channels[0].data, cmap=color_map.reversed(), interpolation='nearest')
		plt.show()

	def dbg_rec_channels(self):
		print('[dbg] rec_channels')
		print('\tframe id: {}'.format(self.signals.idx))
		import matplotlib.pyplot as plt
		color_map = plt.cm.get_cmap('Greys')
		plt.imshow(self.buff_reconstructed_channels[0].data, cmap=color_map.reversed(), interpolation='nearest')
		plt.show()
