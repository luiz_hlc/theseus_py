import math

from standard.frame import stdFrame as Frame
from standard.frame import FrameSignals as Signals
from structures.containers import SharedContainer as Container
class Hierarchy_Controller:
	def __init__(self, v_cfg, partitioning_f, parser_f, initial_f=0):
		self.initial_f = initial_f
		self.parser_f = parser_f
		self.count_f = -1
		self.gop_structure= [[0, 1, 2, 3, 4, 5, 6, 7]] #this represents the frame hierarchy
		self.gop_order = [0, 1, 2, 3, 4, 5, 6, 7] #used for indexing, ust respect gop_structure
		# self.gop_structure= [[0,1,2,3,4,5,6,7,8,9,10,11,12,13,14,15]]
		# self.gop_order= [0,1,2,3,4,5,6,7,8,9,10,11,12,13,14,15]
		self.gop_size = len(self.gop_order)

		yShape = [v_cfg.height, v_cfg.width]
		chShape = [v_cfg.chroma_height, v_cfg.chroma_width]
		self.frameSignals = [
			Signals(0, yShape, chShape, 0, []),
			Signals(1, yShape, chShape, 0, []),
			Signals(2, yShape, chShape, 0, []),
			Signals(3, yShape, chShape, 0, []),
			Signals(4, yShape, chShape, 0, []),
			Signals(5, yShape, chShape, 0, []),
			Signals(6, yShape, chShape, 0, []),
			Signals(7, yShape, chShape, 0, [])
			# Signals(8, yShape, chShape, 0, [])
			# Signals(9, yShape, chShape, 0, []),
			# Signals(10, yShape, chShape, 0, []),
			# Signals(11, yShape, chShape, 0, []),
			# Signals(12, yShape, chShape, 0, []),
			# Signals(13, yShape, chShape, 0, []),
			# Signals(14, yShape, chShape, 0, []),
			# Signals(15, yShape, chShape, 0, [])
			]
		self.frame_buffer = [Frame(self.frameSignals[i], Container) for i in range(self.gop_size)]
		for f in self.frame_buffer: f.split_CTUs(0, partitioning_f) 

	def next_frame(self):
		#ex: gop = 4
		# count_f: 	 0, 1, 2, 3,   4, 5, 6, 7
		# mod_idx:   0, 1, 2, 3,   0, 1, 2, 3
		# gop_idx:   0, 3, 1, 2,   0, 3, 1, 2 #like defined in gop_order
		# gop_count: 0, 0, 0, 0,   1, 1, 1, 1 
		# frame_idx: 0, 3, 1, 2,   4, 7, 5, 6 # + initial frame idx (if it is not 0)
		self.count_f = self.count_f +1
		mod_idx = self.count_f % self.gop_size
		gop_idx = self.gop_order[mod_idx]
		gop_count = int(math.floor(self.count_f/self.gop_size))
		frame_idx = self.initial_f+ gop_count*self.gop_size + gop_idx 
		frame = self.frame_buffer[gop_idx]
		frame.signals.idx = frame_idx
		frame.parse_raw(self.parser_f)
		return frame_idx,gop_idx

	def parse_gop(self, frames=None):
		if frames == None:
			frames = self.gop_size
		f_ids = []
		for _ in range(frames):
			f_id, gop_idx = self.next_frame()
			f_ids.append((f_id, gop_idx))
		return f_ids
