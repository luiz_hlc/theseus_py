import structures.Fields as Fields
import structures.types as types
import numpy as np


class ExampleHeader:
	def __init__(self):
		self.width = Fields.HeaderSimpField('width',2, np.uint16)
		self.height = Fields.HeaderSimpField('height',2, np.uint16)
		# self.channels = Fields.HeaderSimpField('channels',1)
		self.subsampling = Fields.HeaderSimpField('subsampling',1, np.uint8)
		self.nbytes = Fields.HeaderSimpField('nbytes',1, np.uint8)

	def write(self, file):
		self.width.write(file)
		self.height.write(file)
		self.subsampling.write(file)
		self.nbytes.write(file)

	def read(self, file):
		self.width.read(file)
		self.height.read(file)
		self.subsampling.read(file)
		self.nbytes.read(file)

def ExampleStandard(filename, video_cfg=None):
	class Encoder:
		def __init__(self, video_cfg, out_filename):
			self.h = ExampleHeader()
			self.file = open(out_filename, 'wb')
			self.h.width.v = video_cfg.width
			self.h.height.v = video_cfg.height
			# self.h.channels.v = 3
			self.h.subsampling.v = video_cfg.sub_sampling.value
			self.h.nbytes.v = 1 if video_cfg.bit_depth == 8 else 2

			total_size = np.multiply(self.h.width.v, self.h.height.v, dtype=np.int)
			self.y_df = Fields.DataField('Y_frame', self.h.nbytes.v, total_size, np.uint8)

		def writeHeader(self):
			self.h.write(self.file)

		def write_yData(self, data):
			self.y_df.v = data
			self.y_df.write(self.file)
				
	class Decoder:
		def __init__(self, in_filename):
			self.h = ExampleHeader()
			self.file = open(in_filename, 'rb')
			self.h.read(self.file)

			total_size = np.multiply(self.h.width.v, self.h.height.v, dtype=np.int)
			self.y_df = Fields.DataField('Y_frame', self.h.nbytes.v, total_size, np.uint8)

		def read_yData(self):
			self.y_df.read(self.file)
			matrix = np.reshape(self.y_df.v, (self.h.height.v, self.h.width.v))
			import matplotlib.pyplot as plt
			color_map = plt.cm.get_cmap('Greys')
			plt.imshow(matrix, cmap=color_map.reversed(), interpolation='nearest')
			plt.show()

	if video_cfg:
		return Encoder(video_cfg, filename)
	else:
		return Decoder(filename)
	