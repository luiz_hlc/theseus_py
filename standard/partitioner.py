from structures.data import Region
from structures.data import Block
from partitioning.partitioner import RegionPartitioner as Partitioner
from structures.types import Channel
from standard.ctu import CTU

class stdPartitioner:
	def __init__(self, v_cfg, ctu_size):
		
		self.lumma_region = Region(v_cfg.width, v_cfg.height)
		self.chroma_region = Region(v_cfg.chroma_width, v_cfg.chroma_height)
		self.ctu_region = Region(ctu_size, ctu_size)
		self.lumma_regions = Partitioner(self.lumma_region, self.ctu_region)
		self.chroma_regions = Partitioner(self.chroma_region, self.ctu_region)

	def slice_from_frame(self, frame, region):
		x_begin = region.x
		x_end = region.x + region.width
		y_begin = region.y
		y_end = region.y + region.height
		return frame[y_begin:y_end, x_begin:x_end]

	def ctu_hard_split(self, frame, channel):
		"""
			Create the CTU Units with their respective data references
		"""
		if channel == Channel.Y.value:
			regions = self.lumma_regions
		else:
			regions = self.chroma_regions

		ctu_matrix = []
		for r_line in regions:
			ctu_line = []
			for region in r_line:
				original = self.slice_from_frame(frame.buff_ori_channels[channel].data, region)
				pred = self.slice_from_frame(frame.buff_predicted_channels[channel].data, region)
				temp = self.slice_from_frame(frame.buff_temp_channels[channel].data, region)
				reconstructed = self.slice_from_frame(frame.buff_reconstructed_channels[channel].data, region)
				ctu_line.append(CTU(frame, channel, region, original, pred, temp, reconstructed))
			ctu_matrix.append(ctu_line)
		return ctu_matrix

	def ctu_soft_split(self, frame, channel):
		"""
			create the regions associated with each CTU
		"""
		if channel == Channel.Y.value:
			return self.lumma_regions
		else:
			return self.chroma_regions

	def ctu_from_region(self, frame, channel, region):
		original = self.slice_from_frame(frame.buff_ori_channels[channel].data, region)
		pred = self.slice_from_frame(frame.buff_predicted_channels[channel].data, region)
		temp = self.slice_from_frame(frame.buff_temp_channels[channel].data, region)
		reconstructed = self.slice_from_frame(frame.buff_reconstructed_channels[channel].data, region)
		return CTU(frame, channel, region, original, pred, temp, reconstructed)
