import math
from multiprocessing import Process, Semaphore, Pipe
import time
import queue
import copy
class FrameWorker(Process):
	def __init__(self, tid, recv_mutex, send_mutex, pipe_conn, frame_buffer, model):
		Process.__init__(self)
		self.conn = pipe_conn
		self.tid = tid
		self.frame_buffer = frame_buffer
		self.model = copy.deepcopy(model)
		self.recv_mutex = recv_mutex
		self.send_mutex = send_mutex
		
	def run(self):
		while True:
			# Wait for frame to process
			self.recv_mutex.acquire()
			frame_id, gop_id = self.conn.recv()
			self.recv_mutex.release()
			if frame_id == -1:
				# No more to do
				break

			# process frame
			frame = self.frame_buffer[gop_id]
			frame.signals.idx = frame_id
			self.model.process(frame)

			# finish frame
			self.send_mutex.acquire()
			self.conn.send(self.tid)
			self.send_mutex.release()

class WPP_LineWorker(Process):
	def __init__(self, tid, conn, frame_buffer, model, wpp_threads):
		Process.__init__(self)
		self.tid = tid
		self.frame_buffer = frame_buffer
		self.model = model
		self.semphs = None
		# self.smph = Semaphore(value=0)
		# self.next_smph = None
		self.conn = conn
		self.wpp_threads = wpp_threads

	def initialization(frame_buffer, model, wpp_threads):
		# setup all WPP line workers that process a single frame
		pipes = [Pipe() for i in range(wpp_threads)]
		workers = [WPP_LineWorker(i, pipes[i][1], frame_buffer, model, wpp_threads) for i in range(wpp_threads)]
		frame = frame_buffer[0]
		ctu_matrix = frame.ctu_matrix[0]
		total_lines = len(ctu_matrix)
		semphs = [Semaphore(value=0) for i in range(total_lines)]
		for w in workers: w.semphs = semphs 
		# for i in range(wpp_threads-1): 
		# 	workers[i].next_smph = workers[i+1].smph
		# workers[-1].next_smph = workers[0].smph
		parent_pipes = [pipe[0] for pipe in pipes]
		return parent_pipes, workers 

	def run(self):
		while True:
			# Wait for frame to process 
			frame_id, gop_id = self.conn.recv()
			if frame_id == -1:
				# no more to do
				break

			#setup frame info
			frame = self.frame_buffer[gop_id]
			ctu_matrix = frame.ctu_matrix[0]
			total_lines = len(ctu_matrix)
			line_idx = self.tid
			while line_idx < total_lines: 
				# print(f't{time.time()}\tp{self.tid}\tl{line_idx}\tbx\t: init')
				#there are lines to process
				ctu_count = 0
				for ctu in ctu_matrix[line_idx]:
					if line_idx > 0:
						#not first line: wait to start
						# print(f't{time.time()}\tp{self.tid}\tl{line_idx}\tb{ctu_count}\t: lock')
						self.semphs[line_idx].acquire()
					# print(f't{time.time()}\tp{self.tid}\tl{line_idx}\tb{ctu_count}\t: run')

					# process CTU
					real_ctu = self.model.partitioner.ctu_from_region(frame, 0, ctu)
					self.model.process_CTU(real_ctu)
					# frame.dbg_pred_channels(line_idx)


					if ctu_count > 1 and line_idx < (total_lines-1):
						#Next line dependencies are solved:
						# print(f't{time.time()}\tp{self.tid}\tl{line_idx}\tb{ctu_count}\t: release l{line_idx+1}')
						# self.next_smph.release()
						self.semphs[line_idx+1].release()
					
					ctu_count = ctu_count+1

				if line_idx < (total_lines-1):
					#end of line: release the last 2 blocks of next line 
					# print(f't{time.time()}\tp{self.tid}\tl{line_idx}\tb{ctu_count}\t: release 2x l{line_idx+1}')
					# self.next_smph.release()
					# self.next_smph.release()
					self.semphs[line_idx+1].release()
					self.semphs[line_idx+1].release()

				# this worker jumps to next line to process
				line_idx = line_idx + self.wpp_threads

			if line_idx == total_lines: 
				#last line signals that the frame is all done
				self.conn.send(1)

class WPP_FrameWorker(Process):
	def __init__(self, tid, recv_mutex, send_mutex, pipe_conn, frame_buffer, model, wpp_threads=2):
		Process.__init__(self)
		self.conn = pipe_conn
		self.tid = tid
		self.recv_mutex = recv_mutex
		self.send_mutex = send_mutex
		self.child_conn, self.workers = WPP_LineWorker.initialization(frame_buffer, model, wpp_threads)
		for w in self.workers: w.start()
		self.frame_buffer = frame_buffer
		self.wpp_threads = wpp_threads

	def process_frame(self, gop_id, frame_id):
		# setup frame information
			frame = self.frame_buffer[gop_id]
			total_lines = len(frame.ctu_matrix[0])
			last_worker_idx = total_lines%self.wpp_threads
			sync_conn = self.child_conn[last_worker_idx]
			
			# signal child to start
			for child in self.child_conn:
				child.send((frame_id, gop_id))
			
			# wait for last line 
			sync_conn.recv()

	def run(self):
		while True:
			# Wait for frame to process
			self.recv_mutex.acquire()
			frame_id, gop_id = self.conn.recv()
			self.recv_mutex.release()

			if frame_id == -1:
				# No more to do: signal childs
				for child in self.child_conn:
					child.send((-1, -1))
				break

			start = time.time()
			self.process_frame(gop_id, frame_id)
			end = time.time()
			print('f{}\t{}\t{}\t{}'.format(frame_id,start, end, end - start))
			
			# finish frame
			self.send_mutex.acquire()
			self.conn.send(self.tid)
			self.send_mutex.release()



class FramePool_controller:
	def __init__(self, n_workers, frame_buffer, model, wpp_threads=1):
		pipe = Pipe()
		self.conn = pipe[0]
		recv_mutex = Semaphore(value=1)
		send_mutex = Semaphore(value=1)
		if wpp_threads == 1:
			self.workers = [FrameWorker(i,recv_mutex, send_mutex, pipe[1], frame_buffer, model) for i in range(n_workers) ]
		else:
			self.workers = [WPP_FrameWorker(i,recv_mutex, send_mutex, pipe[1], frame_buffer, model, wpp_threads) for i in range(n_workers) ]
		for w in self.workers: w.start()

	def process(self, task_list):
		for t in task_list:
			self.conn.send(t)

		for t in task_list:
			self.conn.recv()


	def finish(self):
		for w in self.workers:
			self.conn.send((-1,-1))
		for w in self.workers:
			w.join()










