import numpy as np
from structures.types import Channel

class CTU:

	def __init__(self, frame, channel, region, original, pred, temp, reconstructed):
		self.frame = frame
		self.channel = channel
		self.region = region
		self.ori_blk = original
		self.pred_blk = pred
		self.temp_blk = temp
		self.reconstructed_blk = reconstructed
		self.qp = frame.signals.qp

	def predict(self, function):
		src = self.ori_blk
		ref = self.frame.buff_reconstructed_channels[self.channel].data
		trg = self.pred_blk
		# print(src)
		function(src, ref, self.region, trg)


	def residue(self, function):
		src_a = self.ori_blk
		src_b = self.pred_blk
		trg = self.temp_blk
		function(src_a, src_b, trg)

	def transform(self, function):
		src = self.temp_blk		
		trg = src
		function(src, trg)

	def quantization(self, function):
		src = self.temp_blk
		trg = src
		function(src, self.qp, trg)

	def inv_quantization(self, function):
		src = self.temp_blk
		trg = src
		function(src, self.qp, trg)

	def inv_transform(self, function):
		src = self.temp_blk		
		trg = src
		function(src, trg)

	def reconstruct(self, function):
		src_a = self.temp_blk
		src_b = self.pred_blk
		trg = self.reconstructed_blk
		function(src_a, src_b, trg)

	# def __doc__(self):



	def dbg_ctu_matrix(self):
		print('[dbg] ctu_matrix')

		print('\tdim: {}, {}'.format(len(self.ctu_matrix), len(self.ctu_matrix[0])))
		y = 0
		for line in self.ctu_matrix[channel]:
			x = 0
			for ctu in line:
				print('\tctu idx:({},{})'.format(y, x))
				print('\tctu size:({},{})'.format(ctu.width, ctu.height))
				ctu.plot()
				x+=1
			y+=1




