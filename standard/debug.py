flag_dbgFrame = True

class dbgFrame:
	def plot(data):
	import matplotlib.pyplot as plt
		color_map = plt.cm.get_cmap('Greys')
		plt.imshow(data, cmap=color_map.reversed(), interpolation='nearest')
		plt.show()

	def channels(frame_id, channels, msg='')
		if not flag_dbgFrame:
			pass
		print('[dbg]'+msg)
		print('\tframe id: {}'.format(frame_id))
		plot(channels[0])
		# plot(channels[1])
		# plot(channels[2])
