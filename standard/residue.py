from residual.residue import ResidualUnity as Residue

class stdResidualUnity:
	class Forward:
		def __init__(self):
			self.f = Residue.Forward().function

		def function(self, in_ori, in_ref, out_ref):
			out_ref[:][:] = self.f(in_ori, in_ref)


	class Inverse:
		def __init__(self):
			self.f = Residue.Inverse().function

		def function(self, in_ori, in_ref, out_ref):
			out_ref[:][:] = self.f(in_ori, in_ref)