from quantization.quantization import Quantizer as Quantizer
from quantization.quantization import HadamardQuantization as QuantMatrix

class stdQuantizer:
	class Control:
		def __init__(self):
			self.matrix_map = QuantMatrix.matrix_map

		def matrix(self, width, height, qp):
			import numpy as np
			if width != height or height%2 != 0:
				return None
			if qp == 0:
				return np.ones(shape=(width, width))
			if qp == 1:
				return self.matrix_map[width]
			if qp >= 2:
				qm = self.matrix_map[width].copy()
				qm = 2*qm
				qm[0][0] = 1
				return qm

	class Forward:
		def __init__(self, control):
			self.f = Quantizer.Forward().function
			self.ctrl = control

		def function(self, in_blk, qp, out_blk):
			h, w = in_blk.shape
			matrix = self.ctrl.matrix(w, h, qp)
			out_blk[:][:] = self.f(matrix, in_blk)


	class Inverse:
		def __init__(self, control):
			self.f = Quantizer.Inverse().function
			self.ctrl = control


		def function(self, in_blk, qp, out_blk):
			h, w = in_blk.shape
			matrix = self.ctrl.matrix(w, h, qp)
			out_blk[:][:] = self.f(matrix, in_blk)