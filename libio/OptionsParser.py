from optparse import OptionParser
import structures.types as types
parser = OptionParser()
parser.add_option("-m", "--mode", dest="mode",
                  help="operation mode: [encode(e), decode(d)]", type= "string", metavar="MODE")
parser.add_option("-i", "--input", dest="in_file",
                  help="read frames from IN_FILE: [str]", type= "string", metavar="IN_FILE")
parser.add_option("-o", "--output", dest="out_file",
                  help="write output to OUT_FILE: [str]", type= "string", metavar="OUT_FILE", default="encoded.bin")
parser.add_option("-W", "--width", dest="frame_width",
                  help="frame WIDTH: [>0]", type="int", metavar="WIDTH")
parser.add_option("-H", "--height", dest="frame_height",
                  help="frame HEIGHT: [>0]", type="int", metavar="HEIGHT")
parser.add_option("-b", "--bitdepth", dest="bit_depth",
                  help="video BITDEPTH [>0]", type="int", metavar="BITDEPTH")
parser.add_option("-s", "--subsampling", dest="sub_sampling",
                  help="chroma SUBSAMPLING: [420,]", type="int", metavar="SUBSAMPLING")
parser.add_option("-f", "--frames", dest="frames",
                  help="number of FRAMES to read [>0, default=1]", type="int", default=1, metavar="FRAMES")
parser.add_option("-c", "--ctuSize", dest="ctuSize",
                  help="Max CTU size. [>0, default=64]", type="int", default=64, metavar="CTU_SIZE")
parser.add_option("-p", "--frameThreads", dest="frameThreads",
                  help="number of parallel frames [>0, default=1]", type="int", default=1, metavar="FRAME_THREADS")
parser.add_option("-w", "--wppThreads", dest="wppThreads",
                  help="Wave-Parallel Processing (WPP): number of lines processed in parallel. 1: WPP disabled. [>0, default=1]", type="int", metavar="WPP_THREADS")

def option_checker(options):
    def check_mode(arg):
        if not arg:
            raise Exception('You must specify the operation mode (flag -m).')
        if arg not in ['e', 'd']: 
            raise Exception("I cannot recognize the operation mode. Expect: ['e', 'd'], Given: {}".format(arg))

    def check_width(arg):
        if not arg:
            raise Exception('You must specify the frame width (flag -W).')
        if not isinstance(arg, int): 
            raise Exception('Frame width must be an integer value.')
        if arg<=0:
            raise Exception('Frame width must be an unsigned value. Given: {}'.format(arg))

    def check_height(arg):
        if not arg:
            raise Exception('You must specify the frame height (flag -H).')
        if not isinstance(arg, int): 
            raise Exception('Frame height must be an integer value.')
        if arg<=0:
            raise Exception('Frame height must be an unsigned value. Given: {}'.format(arg))

    def check_frames(arg):
        if not arg:
            raise Exception('You must specify the number of frames (flag -f).')
        if not isinstance(arg, int): 
            raise Exception('The number of frames must be an integer value.')
        if arg<=0:
            raise Exception('The number of frames must be an unsigned value. Given: {}'.format(arg))

    def check_ctuSize(arg):
        if arg not in [4, 8, 16, 32, 64]:
            raise Exception('Block size not supported. Accepted sizes: [4, 8, 16, 32, 64]. Given: {}'.format(arg))

    def check_input_file(arg):
        import os.path
        if not arg:
            raise Exception('You must specify the input file path (flag -i).')
        if not isinstance(arg, str): 
            raise Exception('The input file name is invalid')
        if not os.path.exists(arg):
            raise Exception('The input file does not exist. Path: {}'.format(arg))

    def check_bitdepth(arg):
        if not arg:
            raise Exception('You must specify the bitdepth (flag -b).')
        if not isinstance(arg, int): 
            raise Exception('The bitdepth must be an integer value.')
        if arg<=0:
            raise Exception('The bitdepth must be an unsigned value. Given: {}'.format(arg))

    def check_subsampling(arg):
        if not arg:
            raise Exception('You must specify the subsampling pattern (flag -s).') 
            # TODO: add more subsampling patterns
        if arg not in [420, 444]:
            raise Exception('The subsampling must be a value in: [420, 444]. Given: {}'.format(arg))

    def check_frameThreads(arg):
        if not arg:
            return
        if arg <= 0:
            raise Exception('The frameThreads must be an unsigned value. Given: {}'.format(arg))

    def check_wppThreads(arg):
        if not arg:
            return
        if arg <= 0:
            raise Exception('The wppThreads must be an unsigned value. Given: {}'.format(arg))


    check_mode(options.mode)
    check_input_file(options.in_file)
    if options.mode == 'e':
        check_height(options.frame_height)
        check_width(options.frame_width)
        check_frames(options.frames)
        check_bitdepth(options.bit_depth)
        check_subsampling(options.sub_sampling)
        check_ctuSize(options.ctuSize)
        check_frameThreads(options.frameThreads)
        check_wppThreads(options.wppThreads)

def parse_arguments():
    (options, args) = parser.parse_args()
    option_checker(options)
    return options