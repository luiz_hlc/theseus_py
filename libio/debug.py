
dbg_ChannelParser = False

class ChannelParser:
	def init(C):
		if dbg_ChannelParser:
			print('[init] ChannelParser')
			print('\tdim: {}, {}'.format(C.w,C.h))
			print('\tsize: {}'.format(C.size))
			print('\tch_offset: {}'.format(C.ch_offset))
			print('\tf_offset: {}'.format(C.f_offset))
			print('\n')

	def read(C, f_id, file_position):
		if dbg_ChannelParser:
			print('[func] ChannelParser.read')
			print('\t-inputs:')
			print('\t\tframe id: {}'.format(f_id))
			print('\t-var:')
			print('\t\tfile position: {}'.format(file_position))
			print('\n')


