import structures.types as types
import numpy as np
import libio.debug as DBG

class ChannelParser:
	"""
		Parse a channel data from the file.
		
	"""
	def __init__(self, filename, w, h, f_offset, ch_offset=0, container=None):
		self.filename = filename
		self.w = w
		self.h = h
		self.size = w*h #TODO: consider number of bytes (?)
		self.ch_offset = ch_offset
		self.f_offset = f_offset
		self.container = container

		# DBG.ChannelParser.init(self)

	def read(self, f_id, container=None):
		file_position = f_id*self.f_offset + self.ch_offset
		file = open(self.filename, 'rb')
		file.seek(file_position)
		data = file.read(self.size)
		file.close()

		# DBG.ChannelParser.read(self, f_id, file_position)
		
		#TODO: consider number of bytes per sample (now is 8 bits only)
		if container is None:
			if self.container is None:
				return np.reshape(np.frombuffer(data, dtype=np.uint8), (self.h, self.w)).astype(dtype=int, copy=False)
			container = self.container
		container[:][:] = np.reshape(np.frombuffer(data, dtype=np.uint8), (self.h, self.w)).astype(dtype=int, copy=False)

class YuvParser():
	"""
		Parse the three different channels data from the file.
		[TODO]: It needs to consider an YUV file with lumma samples only
	"""
	def __init__(self, video_cfg, filename):
		yW, yH = video_cfg.width, video_cfg.height
		cW, cH = video_cfg.chroma_width, video_cfg.chroma_height
		lumma_size = yW*yH
		chroma_size = cW*cH
		cb_offset = lumma_size
		cr_offset = cb_offset + chroma_size
		frame_offset = cb_offset+2*chroma_size
		self.y_parser = ChannelParser(filename, yW, yH, frame_offset)
		self.cb_parser = ChannelParser(filename, cW, cH, frame_offset, cb_offset)
		self.cr_parser = ChannelParser(filename, cW, cH, frame_offset, cr_offset)

	def read(self, f_id, data_buffers):
		"""
			fill the data_buffers with frame 'f_id' index data parsed from the file
		"""
		self.y_parser.read(f_id, data_buffers[0])
		self.cb_parser.read(f_id, data_buffers[1])
		self.cr_parser.read(f_id, data_buffers[2])

# def plot(data):
#     import matplotlib.pyplot as plt
#     color_map = plt.cm.get_cmap('Greys')
#     plt.imshow(data, cmap=color_map.reversed(), interpolation='nearest')
#     plt.show()