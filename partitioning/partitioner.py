import numpy as np
from structures.data import Region

def RegionPartitioner(original, partition):
	"""
		return non-overlapping regions with partition dimensions inside original region.
		regions at the original borders are clipped if neccessary.
	"""
	# calc parameters
	width_blks = int(np.ceil( original.width / partition.width ))
	clip_last_column = True if original.width % partition.width != 0 else False
	height_blks = int(np.ceil( original.height / partition.height ))
	clip_last_line = True if original.height % partition.height != 0 else False

	# construct regions
	r_matrix = []
	y = 0
	ori_w, ori_h = original.width, original.height
	part_w, part_h = partition.width, partition.height
	for i in range(height_blks-1):
		r_line = []
		x = 0
		for j in range(width_blks):
			r = Region(part_w, part_h, x, y)
			r_line.append(r)
			x += part_w
		if clip_last_column:
			r_line[-1].clip(ori_w, ori_h)
		r_matrix.append(r_line)
		y += part_h
	
	x = 0
	r_line = []
	for j in range(width_blks):
		r = Region(part_w, part_h, x, y)
		if clip_last_line:
			r.clip(ori_w, ori_h)
		r_line.append(r)
		x += part_w
	r_matrix.append(r_line)
	return r_matrix
