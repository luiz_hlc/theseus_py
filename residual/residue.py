import numpy as np
import structures.data as data

class ResidualUnity:
	class Forward:
		def __init__(self):
			pass

		def function(self, np_array_ori, np_array_ref):
			# width, height = np_array.shape
			return np_array_ori-np_array_ref

	class Inverse:
		def __init__(self):
			pass

		def function(self, np_array_ori, np_array_ref):
			# width, height = np_array.shape
			return np_array_ori+np_array_ref