Implementação avaliada no Linux
Não há suporte para Windows
	-A lib Multiprocessing opera de forma diferente nos dois SOs

Ambiente:
===============	
1) Instalar conda usando as informações disponíveis em: 
	https://docs.conda.io/projects/conda/en/latest/user-guide/install/index.html

2) Importar o ambiente com as blibliotecas utilizadas no projeto:
	$conda env create -f theseus_py.yml

3) Ativar o ambiente no terminal:
	$source activate theseus_py
	se configurado: $conda activate theseus_py


Execução Sequencial:
===============	
- help (Parametros relativos ao paralelismo são ignorados na versão sequencial)
	$python theseus_sequential.py --help

- Comando genérico:
	$python theseus_sequential.py -m e -i "*PATH/FILE.yuv*" -W *width* -H *height* -f *frames* -b *bitdepth* -s *chroma_subsampling* -c *Block_size*

- Comando para a execução sequencial utilizada no trabalho: 
	$python theseus_sequential.py -m e -i "/home/eclvc/BasketballPass.yuv" -W 416 -H 240 -f 8 -b 8 -s 420 -c 8


Execução paralela:
===============
- help
	$python theseus_parallel.py --help

- Comando genérico:
	$python theseus_sequential.py -m e -i "*PATH/FILE.yuv*" -W *width* -H *height* -f *frames* -b *bitdepth* -s *chroma_subsampling* -c *Block_size* -p *frame_workers* -w *wpp_workers*

- Comando para a execução paralela com 4 workers: 
	$python theseus_parallel.py -m e -i "/home/eclvc/BasketballPass.yuv" -W 416 -H 240 -f 8 -b 8 -s 420 -c 8 -p 4 -w 1


Comentários sobre a implementação:
===============
- existem alguns arquivos diferenciados para as versões sequencial e paralela.
- Arquivos com o prefixo parallel são implementações alternativas àquelas sequenciais
- O principal arquivo da versão sequencial é: __standard/model.py__
- O arquivo principal da versão paralela é: __standard/parallel_model.py__
	- Entretanto, a implementação do paralelismo se encontra em: __standard/parallel.py__  


Características do projeto atual:
===============
- Formato de arquivo YCbCr
- sub-amostragem 420
- tamanhos de bloco fixos: 4x4, 8x8, 16x16, 32x32, 64x64
- GOP e hierarquia são hardcoded: 1 nível e 8 quadros
- Processamento só opera sobre o canal Y
- bits por amostra (bitdepth): 8
- Predição intra: 35 modos (não parametrizados, mas editável em intra/intra.py: IntraCore.modes)
- Processamento paralelo de quadros
- Processamento paralelo de linhas de bloco (WPP)
	- O parâmetro do WPP não pode ultrapassar o número de blocos