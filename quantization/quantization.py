import numpy as np
import structures.data as data
class HadamardQuantization:
	"""
		An arbitrary quantization coefficient matrix just to test the implementation
		[TODO] add other block sizes
	"""
	matrix_map = {8:
		  np.array([[1, 4, 2, 4, 1, 4, 2, 4],
					[4, 8, 8, 8, 8, 8, 8, 8],
					[2, 8, 4, 8, 4, 8, 4, 8],
					[4, 8, 8, 8, 8, 8, 8, 8],
					[1, 8, 4, 8, 2, 8, 4, 8],
					[4, 8, 8, 8, 8, 8, 8, 8],
					[2, 8, 4, 8, 4, 8, 4, 8],
					[4, 8, 8, 8, 8, 8, 8, 8]])
	}

class Quantizer:
	"""
		Quantization module
	"""
	class Forward:
		def __init__(self):
			pass

		def function(self, q_coeffs, samples):
			"""
				perform quantization operation on "samples" considering the "q_coeffs" coefficients
				[TODO] currently, if the block is not square or divisible by 2 size, just skips
			"""
			width, height = samples.shape
			if width != height or height%2 != 0:
				return samples
			q_coeffs = 1/q_coeffs
			return (np.multiply(q_coeffs, samples)).astype(dtype=int)

	class Inverse:
		def __init__(self):
			pass

		def function(self, q_coeffs, samples):
			width, height = samples.shape
			if width != height or height%2 != 0:
				return samples
			return (np.multiply(q_coeffs, samples)).astype(dtype=int)