from dataclasses import dataclass
from enum import Enum
import numpy as np
from structures.debug import Types as DBG


class Subsampling(Enum):
	# TODO: add all subsamplings
	_420 = 1
	_444 = 2

	def convert(value):
		if value == 420:
			return Subsampling._420
		elif value == 444:
			return Subsampling._444
		raise ValueError

class Channel(Enum):
	Y = 0
	Cb = 1
	Cr = 2

def calc_dimensions(channel, sub_sampling, w, h):
	# TODO: add all subsamplings
	if channel == Channel.Y or sub_sampling == Subsampling._444:
		return w,h
	if sub_sampling == Subsampling._420:
		return w>>1, h>>1

	return w, h

@dataclass
class VideoCfg:
	def __init__(self, width, height, frames, bits, sub_samp):
		self.width = width
		self.height = height
		self.frames = frames
		self.bit_depth = bits
		self.sub_sampling = Subsampling.convert(sub_samp)
		self.chroma_width, self.chroma_height = calc_dimensions(Channel.Cb, self.sub_sampling, width, height)

		DBG.VideoCfg.init(self)
		# print("teste", self.chroma_width, self.chroma_height)