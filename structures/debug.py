dbg_Types_VideoCfg = False

class Types:
	class VideoCfg:
		def init(C):
			if dbg_Types_VideoCfg:
				print('[intit] VideoCfg:')
				print('\tframes: {}'.format(C.frames))
				print('\tbit_depth: {}'.format(C.bit_depth))
				print('\tsubsamplig: {}'.format(C.sub_sampling))
				print('\tY dim: {}, {}'.format(C.width, C.height))
				print('\tCh dim: {}, {}'.format(C.chroma_width, C.chroma_height))
				print('\n')