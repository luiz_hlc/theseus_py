import numpy as np
class Container:
	def __init__(self, shape=None, dtype='i', np_data=None):
		if np_data == None:
			self.np_data = np.empty(shape, dtype=dtype)
		else: 
			self.np_data = np_data

	@property
	def data(self):
		return self.np_data

# [TODO]: Find a better solution for SharedContainer
from multiprocessing import RawArray
class SharedContainer:
	def __init__(self, shape, dtype='i'):
		self.shape = shape
		self.dtype = dtype 
		self.raw_data = RawArray(dtype, shape[0]*shape[1])
		# Wrap raw_data as an numpy array so we can easily manipulate its data.
		# Copy data to our shared array.
		# return self

	# def fromRaw(self, shape, raw_data):
	#     self.shape = shape
	#     self.raw_data = raw_data
	#     # Wrap raw_data as an numpy array so we can easily manipulate its data.
	#     self.np_data = np.frombuffer(self.raw_data).reshape(shape)
	#     # Copy data to our shared array.
	#     return self

	@property
	def data(self):
		return np.frombuffer(self.raw_data, dtype = self.dtype).reshape(self.shape)

