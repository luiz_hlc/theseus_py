import numpy as np

class HeaderSimpField:
	def __init__(self,idx, nbytes, dtype, value=0):
		self.idx = idx
		self.bytes = nbytes
		self.v = value
		self.type = dtype

	def write(self, file):
		file.write(np.array(self.v, self.type).tobytes())

	def read(self, file):
		data = file.read(self.bytes)
		self.v = np.frombuffer(data, self.type)[0]
		# print(self.idx+':', self.v)

class DataField:
	def __init__(self, idx, nbytes, total_size, dtype, values=[]):
		self.idx = idx
		self.v = values
		self.bytes = nbytes
		self.total_size = total_size
		self.type = dtype

	def write(self, file):
		file.write(np.array(self.v, self.type).tobytes())

	def read(self, file):
		data = file.read(self.total_size*self.bytes)
		self.v = np.frombuffer(data, self.type)
		# print(self.idx+':', self.v)