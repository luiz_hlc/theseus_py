import numpy as np

class Region:
	def __init__(self, w, h, x=0, y=0):
		self.width = w
		self.height = h
		self.x = x
		self.y = y

	def clip(self, max_w, max_h):
		width = self.x + self.width
		height = self.y + self.height
		if width > max_w:
			self.width = self.width - (width-max_w)

		if height > max_h:
			self.height = self.height - (height-max_h)

	def inside(self, region_b):
		if self.x < region_b.x or self.y < region_b.y:
			return False

		if self.x + self.width > region_b.x + region_b.width or	self.y + self.height > region_b.y + region_b.height:
			return False
		return True

class Block(Region):
	def __init__(self, w, h, container=None, x=0, y=0):
		super().__init__(w, h, x, y)
		if container is None:
			self.container = np.array(w, h)
		else:
			self.container = container

	def init_from_Region(region, container=None):
		return Block(region.width, region.height, container, region.x, region.y)

	def __getitem__(self, pair):
		if pair[0] >= self.width or pair[1]>= self.height:
			raise IndexError

		return self.container[self.y+pair[1]][self.x+pair[0]]

	def __str__(self):
		s = '['
		for y in range(self.height-1):
			s += '['
			for x in range(self.width-1):
				s += str(self[x,y])+','
			s += str(self[self.width-1,y])+'],\n'
		s += '['
		for x in range(self.width-1):
			s += str(self[x,self.height-1])+','
		s += str(self[self.width-1,self.height-1])+']]'
		return s

	def plot(self):
		import matplotlib.pyplot as plt
		color_map = plt.cm.get_cmap('Greys')
		x_begin = self.x
		x_end = self.x+self.width+1
		y_begin = self.y
		y_end = self.y+self.height+1
		plt.imshow(self.container.data[y_begin:y_end, x_begin:x_end], cmap=color_map.reversed(), interpolation='nearest')
		plt.show()


from copy import deepcopy
class Prediction:
	def __init__(self, signal=None, candidate=None):
		self.cost = np.iinfo(np.int32).max
		self.signal = signal
		self.candidate = candidate

	def update_if_better(self, pred):
		if pred.cost < self.cost:
			self.cost = pred.cost
			self.signal = deepcopy(pred.signal)
			self.candidate = deepcopy(pred.candidate)